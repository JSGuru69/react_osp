import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import rules from '../../data/rules';
import { Accordion,Panel } from 'react-bootstrap';



class ManageRules extends Component {
    constructor(props) {
        super(props);
        [].forEach((name) => this[name] = this[name].bind(this))
        this.state={
            activeIndex:-1
        }
    }

    handlePanelClick(index,event){
        let activeindex=-1;
        if(event.target.nodeName=="A"){
            if(event.target.className != ""){
                activeindex=index;
            }
        }
        else if(event.target.parentNode.className!=""){
            activeindex=index;
        }
        this.setState({activeIndex:activeindex});

    }

    render() {

        const { activeIndex } = this.state;

        return (
            <div className="dashboard">
                <div className="sidebar">
                    <ul>
                        <li>
                            <Link to="rules" activeClassName="active">
                                <i className="material-icons">assignment</i>
                                <span>Manage Rules</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="apis">
                                <i className="material-icons">cloud</i>
                                <span>Manage APIs</span>
                            </Link>
                        </li>
                        <li>
                            <a>
                                <i className="material-icons">directions</i>
                                <span>Manage Workflow</span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <i className="material-icons">people_outline</i>
                                <span>Manage <br />Access & Roles</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://portal.azure.com" target="_blank">
                                <i className="material-icons">device_hub</i>
                                <span>Manage Infrastructure</span>
                            </a>
                        </li>

                    </ul>
                </div>

                <div className="dashboard-content">
                    <div className="title">
                        Copyright Infringement Admin: Manage Rules
                    </div>
                    <div className="search-form">
                        <i className="material-icons">mic</i>
                        <input type="text" placeholder="Search by Name, Title, URL Address"/>
                        <i className="material-icons search-button">search</i>
                    </div>
                    <div className="table-container">
                        <div className="col-md-12 text-right">
                            <button>
                                <i className="fa fa-plus"></i>
                                Add Rule
                            </button>
                        </div>

                        <div className="col-md-12 tabs manage-rules">
                            <Accordion>
                                {
                                    rules && rules.map((item, index)=>
                                        <Panel className={index==activeIndex?"tab-open":"tab-close"} key={index} eventKey={index} header={
                                            <div className="header">
                                                { item.ruleName }
                                                <i className="material-icons sunny">wb_sunny</i>
                                                <i className="material-icons arrow-down">{index==activeIndex?"keyboard_arrow_down":"keyboard_arrow_right"}</i>
                                                <span>Active</span>
                                            </div>
                                        } onClick={this.handlePanelClick.bind(this,index)}>
                                            <div className="tab-open">
                                                <div className="wrapper">
                                                    <div className="row">
                                                        <div className="col-md-8">
                                                        </div>
                                                        <div className="col-md-4 text-right">
                                                            <button>
                                                                Edit
                                                            </button>
                                                        </div>
                                                        <div className="clearfix"></div>
                                                        <div className="col-md-12 table-cont">
                                                            <table className="table table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th><span>IDENTIFIER</span></th>
                                                                    <th><span>GROUP</span></th>
                                                                    <th><span>SUB GROUP</span></th>
                                                                    <th><span>FUNCTIONAL AREA</span></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>{ item.rulerIdentifier }</td>
                                                                    <td>{ item.ruleGroup }</td>
                                                                    <td>{ item.subGroup }</td>
                                                                    <td>{ item.functionalArea }</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div className="clearfix"></div>
                                                        <div className="col-md-12 rule-name">
                                                            <b>RULE NAME </b><br/> { item.ruleName }
                                                        </div>
                                                        <div className="clearfix"></div>
                                                        <div className="col-md-12 rule-description">
                                                            <b>RULE DESCRIPTION </b><br/>{ item.ruleDescription }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Panel>
                                    )
                                }
                            </Accordion>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    const {dataReducer, dataAction} = state;
    return {dataReducer, dataAction}
}
export default connect(mapStateToProps)(ManageRules);