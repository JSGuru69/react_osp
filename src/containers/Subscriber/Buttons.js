import React, { Component } from 'react';
import { FormGroup, Table, Row, Col } from 'react-bootstrap';
import Checkbox from './Checkbox';
import cx from 'classnames';
import Button from './Button';

class Buttons extends Component {
    state = {
        checkedIndex: -1
    };

    handleDispute = () => {
        const { checkedItems } = this.props;

        if (checkedItems && !checkedItems.length) {
            this.setState({ submissionError: 'Please select at least one item' })
            return;
        }

        this.setState({
            submitShown: true,
            isSubmitted: false,
            submissionError: null
        }, () => {
            setTimeout(() => {
                if (this._container) {
                    this._container.click();
                    this._container.focus();
                }
            }, 300);
        });

        this.props.handleDispute();
    };

    handleSubmit = () => {
        const { checkedIndex, otherReason } = this.state;
        const nonChecked = checkedIndex === -1;
        const noContent = (checkedIndex === 2 && !otherReason);

        if (nonChecked || noContent) {
            this.setState({
                submissionError: nonChecked ? 'Please check one option' : 'Please fill the input'
            });
        } else {
            this.setState({ isSubmitted: true });
            this.props.handleSubmit();
        }
    };

    handleAcknowledge = () => {
        this.setState({
            submitShown: false,
            isSubmitted: false,
            checkedIndex: -1
        });

        this.props.handleAcknowledge();
    };

    toggleCheckboxes = (index) => () => {
        const { checkedIndex } = this.state;

        this.setState({
            checkedIndex: checkedIndex === index ? -1 : index,
            submissionError: null
        });
    };

    changeReason = ({ target: { value } }) => {
        this.setState({
            checkedIndex: 2,
            otherReason: value,
            submissionError: null
        });
    };

    render() {
        const {
            externalError,
            isAcknowledged            
        } = this.props;
        const {
            submitShown,
            otherReason,
            checkedIndex,
            submissionError,
            isSubmitted
        } = this.state;

        return (
            <form>
                {
                    submitShown ? (
                        <FormGroup className="dispute-container">
                            <Row>
                                <Col xs={3}>
                                    <Checkbox
                                        onChange={ this.toggleCheckboxes(0) }
                                        checked={ checkedIndex === 0 }
                                    >I owe this content</Checkbox>
                                </Col>
                                <Col xs={3}>
                                    <Checkbox
                                        onChange={ this.toggleCheckboxes(1) }
                                        checked={ checkedIndex === 1 }
                                    >Incorrect notice</Checkbox>
                                </Col>
                                <Col xs={6}>
                                    <Checkbox
                                        onChange={ this.toggleCheckboxes(2) }
                                        checked={ checkedIndex === 2 }
                                    >
                                        <input
                                            type="text"
                                            ref={ (c) => this._container = c }
                                            value={ otherReason }
                                            onChange={ this.changeReason }
                                            className="other"
                                            placeholder="Other, please specify"
                                        />
                                    </Checkbox>
                                </Col>
                            </Row>
                        </FormGroup>
                    ) : null
                }
                <div className="buttons">
                    {
                        submitShown ? (
                            <Button
                                tabIndex="-1"
                                className={ cx('default', { selected: isSubmitted }) }
                                onClick={ this.handleSubmit }
                            >Submit</Button>
                        ) : (
                            <Button
                                tabIndex="-1"
                                className="default"
                                onClick={ this.handleDispute }
                            >Dispute</Button>
                        )
                    }
                    <Button
                        className={ cx('acknowledge', { selected: isAcknowledged }) }
                        onClick={ this.handleAcknowledge }
                    >
                        Accept
                    </Button>
                    <div className={ cx('validation-error', { 'hidden': !(submissionError || externalError) }) }>
                        { submissionError || externalError }
                    </div>
                </div>
            </form>
        );
    }
}

export default Buttons;
