import React, { PropTypes } from 'react';
import uniqueId from 'lodash.uniqueid';

const Checkbox = ({ children, checked, disabled, onChange }) => {
    const id = uniqueId();

    return (
        <div className="subscriber-checkbox-container">
            <input
                id={ id }
                type="checkbox"
                className="subscriber-checkbox"
                checked={ checked }
                disabled={ disabled }
                onChange={ onChange }
            />
            <label
                htmlFor={ id }
                onClick={ disabled ? (e) => e.stopPropagation() : () => {} }
            >{ children }</label>
        </div>
    );
};

Checkbox.displayName = 'Checkbox';

Checkbox.propTypes = {
    children: PropTypes.any,
    onChange: PropTypes.func
};

export default Checkbox;
