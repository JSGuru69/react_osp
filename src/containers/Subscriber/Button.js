import React, { PropTypes } from 'react';
import { Button as BsButton } from 'react-bootstrap';

const Button = ({ children, className, onClick }) => (
    <div className="subscriber-button">
        <BsButton
            onClick={ onClick }
            className={ className }
        >{ children }</BsButton>
    </div>
);

Button.displayName = 'Button';

Button.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
    onClick: PropTypes.func
};

export default Button;
