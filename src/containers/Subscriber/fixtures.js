/**
 * This should be fetched somehow (API?) and put into redux store.
 */
export default [
    {
        title: `Mario Bianco, Hunter's Cove, 03/25/2017, Youtube Upload`,
        isActive: true,
        type: 'film',
        violation: [
            {
                violatorsName: 'Mario Bianco',
                timeStamp: '03/25/2017 10:15 AM',
                daysInViolation: '7/2',
                urlIpAddress: 'https://www.youtube.com/watch?v=g5kJA0f',
                geoLocation: 'Italy'
            },
            {
                contentOwner: 'Stargazerfilms',
                fileName: 'huntercove.mp4',
                hashValue: 'ty3y21o078oo92d89778cf4z1oc8ov8',
                fileLocation: 'G Cloud'
            }
        ]
    },
    {
        title: `Mario Bianco, Hunter's Cove, 03/25/2017, Google Cloud`,
        isActive: true,
        type: 'book',
        violation: [
            {
                dateOfViolation: '03/25/2017',
                typeOfViolation: 'Transmission',
                dateReceived: '03/26/2017',
                declarationOfAccuracy: 'CAR',
                signed: 'CAR'
            },
            {
                serviceUsed: 'Revision3',
                deviceName: 'Android2309888822',
                deviceIpLan: '192.168.216',
                hashValue: 'ty3y21o078oo92d89778cf4z1oc8ov8'
            }
        ]
    },
    {
        title: `Mario Bianco, Hunter's Cove, 03/25/2017, Google Cloud`,
        isActive: true,
        type: 'music',
        violation: [
            {
                violatorsName: 'Mario Bianco',
                timeStamp: '03/25/2017 10:15 AM',
                daysInViolation: '7/2',
                urlIpAddress: 'https://www.youtube.com/watch?v=g5kJA0f',
                geoLocation: 'Italy'
            },
            {
                contentOwner: 'Stargazerfilms',
                fileName: 'huntercove.mp4',
                hashValue: 'ty3y21o078oo92d89778cf4z1oc8ov8',
                fileLocation: 'G Cloud'
            }
        ]
    },
    {
        title: `Mario Bianco, Hunter's Cove, 03/25/2017, Google Cloud`,
        isActive: true,
        type: 'tvshow',
        violation: [
            {
                dateOfViolation: '03/25/2017',
                typeOfViolation: 'Transmission',
                dateReceived: '03/26/2017',
                declarationOfAccuracy: 'CAR',
                signed: 'CAR'
            },
            {
                serviceUsed: 'Revision3',
                deviceName: 'Android2309888822',
                deviceIpLan: '192.168.216',
                hashValue: 'ty3y21o078oo92d89778cf4z1oc8ov8'
            }
        ]
    }
];
