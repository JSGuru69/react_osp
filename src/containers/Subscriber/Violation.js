import React, { Component, PropTypes } from 'react';
import { Table } from 'react-bootstrap';
import Buttons from './Buttons';

class Violation extends Component {
    static propTypes = {
        onAcknowledge: PropTypes.func,
        onDispute: PropTypes.func,
        onSubmit: PropTypes.func
    };

    state = {
        isAcknowledged: false,
        isSubmitted: false,
        checkedIndex: -1
    };

    handleDispute = () => {
        this.setState({ isAcknowledged: false });
        this.props.onDispute();
    };

    handleSubmit = () => {
        this.props.onSubmit();
    };

    handleAcknowledge = () => {
        this.setState({ isAcknowledged: true });
        this.props.onAcknowledge();
    };

    renderFirstTypeItem = ([
        {
            violatorsName,
            timeStamp,
            daysInViolation,
            urlIpAddress,
            geoLocation
        },
        {
            contentOwner,
            fileName,
            hashValue,
            fileLocation
        }
    ]) => (
        <div>
            <Table>
                <thead>
                    <tr>
                        <th>Violator's Name</th>
                        <th>Time Stamp</th>
                        <th>Days in Violation/ Previous Violations</th>
                        <th>URL/IP Address</th>
                        <th>Geo Location</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{ violatorsName }</td>
                        <td>{ timeStamp }</td>
                        <td>{ daysInViolation }</td>
                        <td>{ urlIpAddress }</td>
                        <td>{ geoLocation }</td>
                    </tr>
                </tbody>
            </Table>
            <Table>
                <thead>
                    <tr>
                        <th>Content Owner</th>
                        <th>File Name</th>
                        <th>Hash Value</th>
                        <th>File Location</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{ contentOwner }</td>
                        <td>{ fileName }</td>
                        <td>{ hashValue }</td>
                        <td>{ fileLocation }</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    );

    renderSecondTypeItem = ([
        {
            dateOfViolation,
            typeOfViolation,
            dateReceived,
            declarationOfAccuracy,
            signed
        },
        {
            serviceUsed,
            deviceName,
            deviceIpLan,
            hashValue
        }
    ]) => (
        <div>
            <Table>
                <thead>
                    <tr>
                        <th>Date of Violation</th>
                        <th>Type of Violation</th>
                        <th>Date Received by ISP</th>
                        <th>Declaration of Accuracy</th>
                        <th>Signed</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{ dateOfViolation }</td>
                        <td>{ typeOfViolation }</td>
                        <td>{ dateReceived }</td>
                        <td>{ declarationOfAccuracy }</td>
                        <td>{ signed }</td>
                    </tr>
                </tbody>
            </Table>
            <Table>
                <thead>
                    <tr>
                        <th>Service Used</th>
                        <th>Device Name</th>
                        <th>Device IP in LAN</th>
                        <th>Hash Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{ serviceUsed }</td>
                        <td>{ deviceName }</td>
                        <td>{ deviceIpLan }</td>
                        <td>{ hashValue }</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    );

    render() {
        const {
            violation,
            onSubmit
        } = this.props;
        const {
            checkedIndex,
            isSubmitted,
            isAcknowledged,
            otherReason,
            submissionError
        } = this.state;

        return (
            <div className="violations" ref={ (c) => this._container = c }>
                {
                    violation[0].violatorsName ?
                        this.renderFirstTypeItem(violation) :
                            this.renderSecondTypeItem(violation)
                }
                <Buttons
                    handleDispute={ this.handleDispute }
                    handleSubmit={ this.handleSubmit }
                    handleAcknowledge={ this.handleAcknowledge }
                    isAcknowledged={ isAcknowledged }
                />
            </div>
        );
    }
}

export default Violation;
