import React, { Component } from 'react';
import { Accordion, Modal, Panel } from 'react-bootstrap';
import { Alert, AlertList } from 'react-bs-notifier';
import Buttons from './Buttons';
import Checkbox from './Checkbox';
import Violation from './Violation';
import fixtures from './fixtures';

/*
    - Acknowledge in MAIN PAGE should work as the way DISPUTE works
    That means it should close the popup
    Also it updates individual items by making them inactive
    Users should also be able to see that when you open the item in accordion
    Users should be able to change it the opening accordion as well
    - When users do DISPUTE for multiple items it does not disappear after submission
    The SUBMIT button stays, it should be DISPUTE again
    - Every time user make a submission we display a growl message
    it would be located at the right top corner inside the popup
*/
class Subscriber extends Component {
    state = {
        activePanel: -1,
        modalShown: true,
        submitShown: false,
        internallyCheckedItems: [],
        externallyCheckedItems: [],
        alerts: [],
        isAcknowledged: false
    };

    handlePanelSelect = (eventKey, e) => {
        // workaround for checkboxes in panel headings
        const { target } = e;
        const { externallyCheckedItems } = this.state;

        if (target.tagName && target.tagName.toLowerCase() === 'label') {
            const checkbox = target.previousSibling;
            if (checkbox)
                this.setState({
                    externalError: null,
                    externallyCheckedItems:
                        externallyCheckedItems.indexOf(eventKey) !== -1 ?
                            externallyCheckedItems.filter((el) => el !== eventKey) :
                                [...externallyCheckedItems, eventKey]
                });

            return;
        }

        const { activePanel } = this.state;

        this.setState({
            activePanel: activePanel === eventKey ? -1 : eventKey
        });
    };

    handleAcknowledge = () => {
        const { alerts, internallyCheckedItems, externallyCheckedItems } = this.state;

        if (!externallyCheckedItems.length) {
            this.setState({ externalError: 'Please select at least one item' });
            return;
        }

        this.setState({
            externalError: null,
            externallyCheckedItems: [],
            internallyCheckedItems: [...internallyCheckedItems, ...externallyCheckedItems],
            alerts: [...alerts, 'Thank you for your submission']
        });
    };

    handleDispute = () => {
        this.setState({
            externalError: null
        });
    };

    handleSubmit = () => {
        const { internallyCheckedItems, externallyCheckedItems } = this.state;

        this.setState({
            externalError: null,
            externallyCheckedItems: [],
            internallyCheckedItems: [...internallyCheckedItems, ...externallyCheckedItems]
        });
    };

    handleAlertDismiss = (idx) => {
        const { alerts } = this.state;
        alerts.splice(idx, 1);

        this.setState({
            alerts: [...alerts]
        });
    };

    handleClose = () => {
        this.setState({
            modalShown: false
        });
    };
    
    renderPanelHeader = (title, type, activePanel, idx) => {
        const {
            internallyCheckedItems,
            externallyCheckedItems
        } = this.state;

        const isActive = activePanel === idx;

        // workaround for getting parent heading and apply style to it
        if (this[`panel${idx}`]) {
            const heading = this[`panel${idx}`].parentNode;

            if (isActive) {
                heading.classList.add('selected');
            } else {
                heading.classList.remove('selected');
            }
        }

        const isInternallyChecked = internallyCheckedItems.indexOf(idx) !== -1;
        const isExternallyChecked = externallyCheckedItems.indexOf(idx) !== -1;

        return (
            <div className="clearfix" ref={ (c) => this[`panel${idx}`] = c }>
                { 
                    (activePanel === -1) ?
                        <Checkbox
                            checked={ isInternallyChecked || isExternallyChecked }
                            disabled={ isInternallyChecked }
                        /> : null
                }
                <span className="title">{ title }</span>
                { this.renderViolationType(type) }
                <span className="panel-buttons pull-right">
                    {
                        isActive ? (
                            <span className="arrow-down" />
                        ) : (
                            <span className="arrow-right" />
                        )
                    }
                    <span className="state">Active</span>
                    <span className="sunny" />
                </span>
            </div>
        );
    };

    renderViolationType = (type) => {
        return ( <span className={ `violation-${type}`} /> );
    };

    closeItem = (idx) => () => {
        const { internallyCheckedItems } = this.state;
        const newInternallyCheckedItems = [...internallyCheckedItems, idx];

        this.setState({
            activePanel: -1,
            internallyCheckedItems: newInternallyCheckedItems
        });
    };

    clearItem = (idx) => () => {
        const { internallyCheckedItems } = this.state;

        this.setState({
            internallyCheckedItems: internallyCheckedItems.filter((el) => el !== idx),
            externallyCheckedItems: internallyCheckedItems.filter((el) => el !== idx)
        });
    };

    renderViolation = (violation, idx) => {
        return (
            <Violation
                violation={ violation }
                onAcknowledge={ this.closeItem(idx) }
                onDispute={ this.clearItem(idx) }
                onSubmit={ this.closeItem(idx) }
            />
        );
    };

    render() {
        const {
            activePanel,
            modalShown,
            internallyCheckedItems,
            externallyCheckedItems,
            alerts,
            isAcknowledged,
            externalError
        } = this.state;

        return (
            <div className="subscriber">
                <Modal
                    backdrop
                    className="subscriber-modal"
                    show={ modalShown }
                >
                    <Modal.Header>
                        <Modal.Title>
                            <span className="close-custom" onClick={ this.handleClose } />
                            <div className="title">Copyright Infringment Notice</div>
                            <div className="description">Your accound has been flagged due to multiple alleged copyright violation notices. Please check below to see more details</div>
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <AlertList
                            timeout={ 3000 }
                            onDismiss={ this.handleAlertDismiss }
                            alerts={
                                alerts.map((message, i) => ({ id: i, type: 'success', message }))
                            }
                        />
                            
                        <Accordion activeKey={ activePanel }>
                            {
                                fixtures.map(({ title, isActive, type, violation }, idx) => {
                                    return (
                                        <Panel
                                            key={ idx }
                                            header={
                                                this.renderPanelHeader(
                                                    title,
                                                    type,
                                                    activePanel,
                                                    idx
                                                )
                                            }
                                            eventKey={ idx }
                                            onSelect={ this.handlePanelSelect }
                                        >
                                            { violation ? this.renderViolation(violation, idx) : <span /> }
                                        </Panel>
                                    );
                                })
                            }
                        </Accordion>
                        {
                            (activePanel === -1) ? (
                                <Buttons
                                    handleSubmit={ this.handleSubmit }
                                    handleDispute={ this.handleDispute }
                                    handleAcknowledge={ this.handleAcknowledge }
                                    checkedItems={ externallyCheckedItems }
                                    isAcknowledged={ isAcknowledged }
                                    externalError={ externalError }
                                />
                            ) : null
                        }
                    </Modal.Body>
                </Modal>
            </div>  
        );
    }
}

export default Subscriber;
