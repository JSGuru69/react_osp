import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import '../assets/styles/landing.scss';
import Footer from '../components/Footer/Footer';

class Landing extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
                <div className="viewer">
                    <div className="viewer-header">
                        <div className="container">
                            <div className="contact-title">
                                {/*<img className="logoimg" src="/src/assets/images/logo.gif" />*/}
                                <h1>Copyright Infringement Submission</h1>
                            </div>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="viewer-blog" >
                                <Link className="viewer-box" to='/submission'>
                                    <h6>Notice <span>Submitter</span></h6>
                                </Link>
                                <Link className="viewer-box" to='/dashboard'>
                                    <h6>Copyright <span>Team</span> </h6>
                                </Link>
                                <Link className="viewer-box" to='/subscriber'>
                                    <h6>End Customer</h6>
                                    <p>(subscriber, account<br />holder etc)</p>
                                </Link>
                                <Link className="viewer-box" to='/analytics'>
                                    <h6>Copyright Analytics</h6>
                                </Link>
                                <Link className="viewer-box" to='/rules' >
                                    <h6>Platform<br />Admin</h6>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
        );
    }
}


function mapStateToProps(state) {
    const {dataReducer, dataAction} = state;
    return {dataReducer, dataAction}
}
export default connect(mapStateToProps)(Landing);