import React, {Component} from 'react';
import {connect} from 'react-redux';
import { browserHistory } from 'react-router';
import '../assets/styles/login.scss'


class Login extends Component {
    constructor(props) {
        super(props);
    }

    handleLogin(){
        browserHistory.push('/landing');
    }

    render() {

        return (
            <div className="login-container" style={{border:"1px red solid"}}>
                <div className="flex-container login-div">
                    <div className="login-wrap">
                        <div>
                            <div className="login-title">LOGIN</div>
                        </div>
                        <div>
                            <button className="login-button" onClick={this.handleLogin}>Login using DMCA Credentials
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    const {dataReducer, dataAction} = state;
    return {dataReducer, dataAction}
}
export default connect(mapStateToProps)(Login);