import React, {Component} from 'react';
import { browserHistory } from 'react-router';
import {connect} from 'react-redux';
import '../../assets/styles/submissions.scss';
import '../../assets/styles/subscriber.scss';

import {
    Accordion,
    Modal,
    Panel
} from 'react-bootstrap';
class ManualSubmission extends Component {
    constructor(props) {
        super(props);
        const mail_to='notificationabusedev@gmail.com';
        const Subject='Notice%20of%20Claimed%20Infringement%20Case%20ID';
        const body='-----BEGIN%20PGP%20SIGNED%20MESSAGE-----%0AHash%3A%20SHA1%0A---------------%20Infringement%20Details%20----------------------------------%0ATitle%3A%20-content-%0ATimestamp%3A%20-timestamp-Z%0AIP%20Address%3A%20XXX.XXX.XXX%0AWeb%20Url%3A%20https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Dg5kJA0f%0APort%3A%20-port-%0AType%3A%20BitTorrent%0AHash%3A%20-hash-%0AFilename%3A%20-filename-%0AFilesize%3A%20XXX%20MB%0A-----------------------------------------------------------------------------%0A%0ADear%20ISP%20Provider%0A%0AWe%20have%20received%20information%20that%20an%20subscriber%20who%20owns%20your%20IP%20address%20has%20downloaded%20the%20copyrighted%20material.%0A%0AThe%20distribution%20of%20unauthorized%20copies%20of%20copyrighted%20content%20constitutes%20copyright%20infringement%20under%20the%20Copyright%20Act%2C%20Title%2017%20United%20States%20Code%20Section%20106(3).%20This%20conduct%20may%20also%20violate%20the%20laws%20of%20other%20countries%2C%20international%20law%2C%20and%2For%20treaty%20obligations.%0A%0ASince%20you%20own%20this%20IP%20address%20below%2C%20we%20request%20that%20you%20immediately%20do%20the%20following%3A%0A%0A1)%20Contact%20the%20subscriber%20who%20has%20engaged%20in%20the%20conduct%20described%20above%20and%20take%20steps%20to%20prevent%20the%20subscriber%20from%20further%20downloading%20or%20uploading%20the%20copyright%20content%20without%20authorization.%0A%0A2)%20Take%20appropriate%20action%20against%20the%20account%20holder%20under%20your%20Abuse%20Policy%2FTerms%20of%20Service%20Agreement.%0A%0AWe%20own%20the%20exclusive%20rights%20in%20the%20copyrighted%20material%20at%20issue%20in%20this%20notice%2C%20we%20hereby%20state%20that%20we%20have%20a%20good%20faith%20belief%20that%20use%20of%20the%20material%20in%20the%20manner%20complained%20was%20not%20authorized%2C%20its%20respective%20agents%2C%20or%20the%20law.%0A%0AAlso%2C%20we%20hereby%20state%2C%20under%20penalty%20of%20perjury%2C%20that%20we%20are%20authorized%20to%20act%20on%20behalf%20of%20the%20owner%20of%20the%20exclusive%20rights%20being%20infringed%20as%20set%20forth%20in%20this%20notification.%0A%0AWe%20appreciate%20your%20assistance%20and%20thank%20you%20for%20your%20cooperation%20in%20this%20matter.%20Your%20prompt%20response%20is%20requested.%0A%0AThanks%0AAnti-piracy%20team';
        this.mail_to=`mailto:${mail_to}?subject=${Subject}&body=${body}`;
        this.state={
            contentType:{
                value:''
            },
            ViolationType:{
                value:''
            },
            caseIdentifier:{
                    value:'',
                    error:'',
            },
            owner:{
                value:'',
                error:'',
            },
            ownerEmail:{
                value:'',
                error:'',
            },
            ispName:{
                value:'',
                error:'',
            },
            ispEmail:{
                value:'',
                error:'',
            },
            contentUrl:{
                value:'',
                error:'',
            },
            violationTimestamp:{
                value:'',
                error:'',
            },
            ipAddress:{
                value:'',
                error:'',
            },
            hashValueoffile:{
                value:'',
                error:'',
            },
            title:{
                value:'',
                error:'',
            },fileName:{
                value:'',
                error:'',
            },
            serviceUsed:{
                value:'',
                error:'',
            },
            portNumber:{
                value:'',
                error:'',
            },
            digitalSignature:{
                value:'',
                error:'',
            },
            checkbox1:false,
            checkbox2:false,
            isFormValid:false,
            isShowModel:false,
            isShowSucessModel:false,

        }

        this.HandleInput=this.HandleInput.bind(this);
        this.InputFocus=this.InputFocus.bind(this);
        this.FormSubmit=this.FormSubmit.bind(this);
        this.HandleSelect=this.HandleSelect.bind(this);
        this.HandleCheckBox=this.HandleCheckBox.bind(this);
        this.Save=this.Save.bind(this);
        this.Submit=this.Submit.bind(this);
        this.Edit=this.Edit.bind(this);
        this.sendEmail=this.sendEmail.bind(this);
        this.Logout=this.Logout.bind(this);

    }

    sendEmail(){
        window.open(this.mail_to, '_blank');
    }

    HandleInput(event){
        if(event.target.value.trim()=="") {
            event.target.previousSibling.className = "form-label";
        }
        const Name=event.target.name;
        const value=event.target.value.trim();
        if(value!='') {
            this.setState({
                [Name]: {
                    ...[Name],
                    value: value,
                    error:''
                }
            });
        }else{
            this.setState({
                [Name]: {
                    ...[Name],
                    value: value,
                    error:'error'
                }
            });
        }
    }

    HandleSelect(event){
        const Name=event.target.name;
        this.setState({
            [Name]: {
                ...[Name],
                value: event.target.value,
            }
        });
    }

    InputFocus(event){
        event.target.previousSibling.className="selected"
    }

    FormSubmit(event){
        let isValidform=false;
        if(this.state.checkbox1 && this.state.checkbox2){
            isValidform=true;
        }else{
            this.setState({isShowModel:true,isFormValid:false,});
        }

        if(isValidform && this.state.ispName.value.trim()!="" && this.state.ispEmail.value.trim()!="" && this.state.contentUrl.value.trim()!="" && this.state.violationTimestamp.value.trim()!="" &&
            this.state.ipAddress.value.trim()!="" && this.state.hashValueoffile.value.trim()!="" && this.state.fileName.value.trim()!="" && this.state.serviceUsed.value.trim()!=""){
            this.setState({
                isShowModel:false,
                isFormValid:true,
            });
        }
        else{
            this.setState({
                isFormValid:false,
                isShowModel:true,
            });
        }
    }

    HandleCheckBox(event){
            this.setState({
                [event.target.name]: event.target.checked
            });
    }

    Save(event){
        this.setState({
            isShowSucessModel:true,
        });
    }

    Submit(event){
        browserHistory.push('/submission')
    }

    Logout(event){
        browserHistory.push('/')
    }

    Edit(event){
        this.setState({
            ...this.state,
            isFormValid:false,
        });
    }

    render() {

        return (
            <div className="form_detail">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form_details">
                                <h1>Copyright Infringement Submission:Fill out form</h1>
                                <ul className="nav nav-tabs" role="tablist">
                                    <li role="presentation" className="active">
                                        <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                            <img src="/src/assets/images/fillform-panel.png" alt="" />
                                            <span>Fill Online Form</span>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#profile" onClick={()=>this.sendEmail()} aria-controls="profile" role="tab" data-toggle="tab">
                                            <img src="/src/assets/images/sendemail-panel.png" alt="" />
                                            <span>Send an e-mail</span>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                         <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
                                             <img src="/src/assets/images/bulkreport-panel.png" alt="" />
                                             <span>Report in Bulk</span>
                                         </a>
                                    </li>
                                    <li role="presentation">
                                         <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
                                             <img src="/src/assets/images/sendemail-panel.png" alt="" />
                                             <span>Systems Integration</span>
                                         </a>
                                    </li>
                                </ul>
                                <div className={this.state.isFormValid?"tab-content submitted":"tab-content"}>
                                    <div role="tabpanel" className="tab-pane active" id="home">
                                        <div className="arrow-up"></div>
                                        <div className="form_details_data">
                                            <h4>Content details</h4>
                                            <div className="text_filed">
                                                <div className="row">
                                                    <div className="form-group col-xs-4">
                                                            <div className={this.state.isFormValid?"drop_down displaynone":"drop_down"}>
                                                                <select name="contentType" className="form-control"
                                                                        onChange={this.HandleSelect}>
                                                                    <option value="">PLEASE SELECT CONTENT TYPE</option>
                                                                    <option value="Film">Film</option>
                                                                    <option value="Music">Music</option>
                                                                    <option value="TV SHOW">TV Show</option>
                                                                    <option value="Game">Game</option>
                                                                    <option value="App">App</option>
                                                                    <option value="Image">Image</option>
                                                                    <option value="Book">Book</option>
                                                                </select>
                                                            </div>
                                                             <lable className={this.state.isFormValid?"form-submit-label":"form-submit-label displaynone"}>{this.state.contentType.value}</lable>
                                                        </div>
                                                </div>
                                                <div className="row">

                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">Case identifier</label>
                                                                <input type="text" name="caseIdentifier"
                                                                       className="form-control"
                                                                       onFocus={this.InputFocus}
                                                                       onBlur={this.HandleInput}
                                                                />
                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable className="form-submit-label">{this.state.caseIdentifier.value}</lable>
                                                            </div>

                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">Owner</label>
                                                                <input type="text"
                                                                       className="form-control"
                                                                       name="owner"
                                                                       onFocus={this.InputFocus}
                                                                       onBlur={this.HandleInput}
                                                                />
                                                            </div>
                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.owner.value}</lable>
                                                            </div>

                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">Owner's email</label>
                                                                <input type="text"
                                                                       className="form-control"
                                                                       name="ownerEmail"
                                                                       onFocus={this.InputFocus}
                                                                       onBlur={this.HandleInput}
                                                                />
                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.ownerEmail.value}</lable>
                                                            </div>

                                                </div>
                                            </div>
                                            <h4>Infingement details</h4>
                                            <div className="text_filed">
                                                <div className="row">

                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <div className="drop_down">
                                                                    <select className="form-control"
                                                                            name="ViolationType"
                                                                            onChange={this.HandleSelect}>
                                                                        <option value="">PLEASE SELECT VIOLATION TYPE
                                                                        </option>
                                                                        <option value="Distribution">Distribution
                                                                        </option>
                                                                        <option value="Storage">Storage</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.ViolationType.value}</lable>
                                                            </div>

                                                </div>
                                                <div className="row">

                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">ISP/OSP Name<b className="err">*</b></label>
                                                                <input type="text"
                                                                       className="form-control"
                                                                       name="ispName"
                                                                       onFocus={this.InputFocus}
                                                                       onBlur={this.HandleInput}
                                                                />
                                                                {
                                                                    this.state.ispName.error != '' ?
                                                                        <p className="errorp">Please enter the ISP
                                                                            Name </p> : ''
                                                                }
                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.ispName.value}</lable>
                                                            </div>


                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">ISP/OSP email<b className="err">*</b></label>
                                                                <input type="text"
                                                                       className="form-control"
                                                                       name="ispEmail"
                                                                       onFocus={this.InputFocus}
                                                                       onBlur={this.HandleInput}
                                                                />
                                                                {
                                                                    this.state.ispEmail.error != '' ?
                                                                        <p className="errorp">Please enter the ISP
                                                                            Email </p> : ''
                                                                }

                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.ispEmail.value}</lable>
                                                            </div>

                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">Content URL<b className="err">*</b></label>
                                                                <input type="text"
                                                                       className="form-control"
                                                                       name="contentUrl"
                                                                       onFocus={this.InputFocus}
                                                                       onBlur={this.HandleInput}
                                                                />
                                                                {
                                                                    this.state.contentUrl.error != '' ?
                                                                        <p className="errorp">Please enter the Content
                                                                            URL </p> : ''
                                                                }

                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.contentUrl.value}</lable>
                                                            </div>

                                                </div>
                                                <div className="row">

                                                    <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                        <label className="form-label">violation time stamp<b className="err">*</b></label>
                                                        <input type="text"
                                                               className="form-control"
                                                               name="violationTimestamp"
                                                               onFocus={this.InputFocus}
                                                               onBlur={this.HandleInput}
                                                               />
                                                        {
                                                            this.state.violationTimestamp.error!=''?
                                                                <p className="errorp">Please enter the Violation TimeStamp </p>:''
                                                        }
                                                    </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.violationTimestamp.value}</lable>
                                                            </div>

                                                    <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                        <label className="form-label">IP address<b className="err">*</b></label>
                                                        <input type="text" className="form-control" name="ipAddress"
                                                               onBlur={this.HandleInput}
                                                               onFocus={this.InputFocus}
                                                               />

                                                        {
                                                            this.state.ipAddress.error!=''?
                                                                <p className="errorp">Please enter the Ip Address</p>:''
                                                        }
                                                    </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.ipAddress.value}</lable>
                                                            </div>

                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">Hash value of file<b className="err">*</b></label>
                                                                <input type="text" className="form-control"
                                                                       name="hashValueoffile"
                                                                       onBlur={this.HandleInput}
                                                                       onFocus={this.InputFocus}
                                                                />
                                                                {
                                                                    this.state.hashValueoffile.error != '' ?
                                                                        <p className="errorp">Please enter the Hash
                                                                            value of file</p> : ''
                                                                }

                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.hashValueoffile.value}</lable>
                                                            </div>

                                                </div>
                                                <div className="row">

                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">Title</label>
                                                                <input type="text"
                                                                       className="form-control"
                                                                       name="title"
                                                                       onBlur={this.HandleInput}
                                                                       onFocus={this.InputFocus}
                                                                />

                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.title.value}</lable>
                                                            </div>


                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">File name<b className="err">*</b></label>
                                                                <input type="text"
                                                                       className="form-control"
                                                                       name="fileName"
                                                                       onBlur={this.HandleInput}
                                                                       onFocus={this.InputFocus}
                                                                />
                                                                {
                                                                    this.state.fileName.error != '' ?
                                                                        <p className="errorp">Please enter the File
                                                                            name</p> : ''
                                                                }

                                                            </div>
                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.fileName.value}</lable>
                                                            </div>
                                                            <div className={this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <label className="form-label">Service Used<b className="err">*</b></label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="serviceUsed"
                                                                    onBlur={this.HandleInput}
                                                                    onFocus={this.InputFocus}
                                                                />
                                                                {
                                                                    this.state.serviceUsed.error != '' ?
                                                                        <p className="errorp">Please enter the Service
                                                                            Used</p> : ''
                                                                }

                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-4 displaynone":"form-group col-xs-4"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.serviceUsed.value}</lable>
                                                            </div>

                                                </div>
                                                <div className="row">

                                                            <div className={this.state.isFormValid?"form-group col-xs-12 displaynone":"form-group col-xs-12"}>
                                                                <label className="form-label">Port number</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="portNumber"
                                                                    onBlur={this.HandleInput}
                                                                    onFocus={this.InputFocus}
                                                                />
                                                            </div>

                                                            <div className={!this.state.isFormValid?"form-group col-xs-12 displaynone":"form-group col-xs-12"}>
                                                                <lable
                                                                    className="form-submit-label">{this.state.portNumber.value}</lable>
                                                            </div>

                                                </div>
                                            </div>
                                            <h4>submitter information</h4>
                                            <div className="text_filed">
                                                        <div className={this.state.isFormValid?"form-group col-xs-12 displaynone":"form-group col-xs-12"}>
                                                            <label className="form-label">Digital signature(enter your
                                                                full legal name)</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                name="digitalSignature"
                                                                onBlur={this.HandleInput}
                                                                onFocus={this.InputFocus}
                                                            />
                                                        </div>

                                                        <div className={!this.state.isFormValid?"form-group col-xs-12 displaynone":"form-group col-xs-12"}>
                                                            <lable
                                                                className="form-submit-label">{this.state.digitalSignature.value}</lable>
                                                        </div>

                                                <div className="checkbox">
                                                    <label>
                                                             <div className={this.state.isFormValid?"displaynone":"subscriber-checkbox-container"}>
                                                            <input type="checkbox"
                                                                   id="checkbox1"
                                                                   className="subscriber-checkbox"
                                                                   value="" name="checkbox1"
                                                                   onChange={this.HandleCheckBox}  />
                                                            <label htmlFor="checkbox1"></label>
                                                            </div>
                                                            Information in this notification is accurate
                                                    </label>
                                                </div>
                                                <div className="checkbox">
                                                    <label>
                                                        <div className={this.state.isFormValid?"displaynone":"subscriber-checkbox-container"}>
                                                            <input type="checkbox"
                                                                   id="checkbox2"
                                                                   className="subscriber-checkbox"
                                                                   value="" name="checkbox2"
                                                                   onChange={this.HandleCheckBox}  />
                                                            <label htmlFor="checkbox2"></label>
                                                        </div>
                                                        The signator above has a good faith belief that use of the
                                                        material in the manner complained of herein is not authorized by
                                                        the copyright owner, its agent, or by operation of law.
                                                    </label>
                                                </div>
                                            </div>
                                            {
                                                !this.state.isFormValid ?
                                                    ''
                                                    :
                                                    <div className="btn_info noborder">
                                                        <button type="submit" className="btn btn-default" onClick={this.Edit}>cancel</button>
                                                        <button type="submit" className="btn btn-default" onClick={this.Edit}>edit</button>
                                                        <button type="submit" className="btn btn-default" onClick={this.Save}>save</button>
                                                        <button type="submit" className="btn btn-default" onClick={this.Save}>submit</button>
                                                    </div>
                                            }
                                        </div>
                                        {
                                            !this.state.isFormValid ?
                                                <div className="btn_info">
                                                    <button type="submit" onClick={this.FormSubmit}
                                                            className="btn btn-default">Next
                                                    </button>
                                                </div>
                                                :
                                                ''
                                        }
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="profile">...</div>
                                    <div role="tabpanel" className="tab-pane" id="messages">...</div>
                                    <div role="tabpanel" className="tab-pane" id="settings">...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    backdrop
                    className="subscriber-modal subscriber-modal"
                    show={ this.state.isShowModel }
                >
                    <Modal.Header>
                        <Modal.Title>
                            <div className="title">Please put your name and select the checkboxes to agree to the terms</div>
                            <div className="description"></div>
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="bottonarea">
                            <div className="btn_info">
                                <button type="submit" onClick={()=>this.setState({isShowModel:false})} className="btn btn-default">Continue</button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
                <Modal
                    backdrop
                    className="subscriber-modal subscriber-modal"
                    show={ this.state.isShowSucessModel }
                >
                    <Modal.Header>
                        <Modal.Title>
                            <div className="title">Submission sucessfull. Thanks you!</div>
                            <div className="description"></div>
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="bottonarea">
                            <div className="btn_info">
                                <button type="submit" onClick={this.Logout} className="btn btn-default">Log out</button>
                                <button type="submit" onClick={this.Submit} className="btn btn-default">Continue</button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}


function mapStateToProps(state) {
    const {dataReducer, dataAction} = state;
    return {dataReducer, dataAction}
}
export default connect(mapStateToProps)(ManualSubmission);