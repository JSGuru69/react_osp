import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import {connect} from 'react-redux';
import '../assets/styles/submission.scss';
const ReactHighcharts = require('react-highcharts');


class Submission extends Component {
    constructor(props) {
        super(props);
        const mail_to='notificationabusedev@gmail.com';
        const Subject='Notice%20of%20Claimed%20Infringement%20Case%20ID';
        const body='-----BEGIN%20PGP%20SIGNED%20MESSAGE-----%0AHash%3A%20SHA1%0A---------------%20Infringement%20Details%20----------------------------------%0ATitle%3A%20-content-%0ATimestamp%3A%20-timestamp-Z%0AIP%20Address%3A%20XXX.XXX.XXX%0AWeb%20Url%3A%20https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Dg5kJA0f%0APort%3A%20-port-%0AType%3A%20BitTorrent%0AHash%3A%20-hash-%0AFilename%3A%20-filename-%0AFilesize%3A%20XXX%20MB%0A-----------------------------------------------------------------------------%0A%0ADear%20ISP%20Provider%0A%0AWe%20have%20received%20information%20that%20an%20subscriber%20who%20owns%20your%20IP%20address%20has%20downloaded%20the%20copyrighted%20material.%0A%0AThe%20distribution%20of%20unauthorized%20copies%20of%20copyrighted%20content%20constitutes%20copyright%20infringement%20under%20the%20Copyright%20Act%2C%20Title%2017%20United%20States%20Code%20Section%20106(3).%20This%20conduct%20may%20also%20violate%20the%20laws%20of%20other%20countries%2C%20international%20law%2C%20and%2For%20treaty%20obligations.%0A%0ASince%20you%20own%20this%20IP%20address%20below%2C%20we%20request%20that%20you%20immediately%20do%20the%20following%3A%0A%0A1)%20Contact%20the%20subscriber%20who%20has%20engaged%20in%20the%20conduct%20described%20above%20and%20take%20steps%20to%20prevent%20the%20subscriber%20from%20further%20downloading%20or%20uploading%20the%20copyright%20content%20without%20authorization.%0A%0A2)%20Take%20appropriate%20action%20against%20the%20account%20holder%20under%20your%20Abuse%20Policy%2FTerms%20of%20Service%20Agreement.%0A%0AWe%20own%20the%20exclusive%20rights%20in%20the%20copyrighted%20material%20at%20issue%20in%20this%20notice%2C%20we%20hereby%20state%20that%20we%20have%20a%20good%20faith%20belief%20that%20use%20of%20the%20material%20in%20the%20manner%20complained%20was%20not%20authorized%2C%20its%20respective%20agents%2C%20or%20the%20law.%0A%0AAlso%2C%20we%20hereby%20state%2C%20under%20penalty%20of%20perjury%2C%20that%20we%20are%20authorized%20to%20act%20on%20behalf%20of%20the%20owner%20of%20the%20exclusive%20rights%20being%20infringed%20as%20set%20forth%20in%20this%20notification.%0A%0AWe%20appreciate%20your%20assistance%20and%20thank%20you%20for%20your%20cooperation%20in%20this%20matter.%20Your%20prompt%20response%20is%20requested.%0A%0AThanks%0AAnti-piracy%20team';
        this.mail_to=`mailto:${mail_to}?subject=${Subject}&body=${body}`;
        this.sendEmail=this.sendEmail.bind(this);
        this.ManualSubmissions=this.ManualSubmissions.bind(this);
        this.prepareChart = this.prepareChart.bind(this);

        this.state = {
            charts:{},
            totalUploadEvents:{}
        }
    }

    sendEmail(){
        window.open(this.mail_to, '_blank');
    }

    ManualSubmissions(){
        browserHistory.push('/submission/manual')
    }

    componentDidMount() {
        fetch('/notificationsupload.json')
            .then(response => {
                return response.json();
            })
            .then(charts => {
                console.log(charts);
                this.setState({ charts });
                this.prepareChart(this.state.charts);
            });

    }

    prepareChart(response){
        let results = response.hits.hits;
        console.log("Uploads: ", results)
        let emailData = new Array(results.length).fill(0);
        let bulkData = new Array(results.length).fill(0);
        let webData = new Array(results.length).fill(0);
        let xAxis = [];

        for (var i in results) {
            if (xAxis.indexOf(results[i]._source.month) == -1) {
                var xdata = results[i]._source.month + ' ' + results[i]._source.year;
                xAxis.push(xdata)
            }
        }

        for (var i in results) {
            var xdata = results[i]._source.month + ' ' + results[i]._source.year;
            for (var x in xAxis) {
                if (xAxis[x] == xdata) {
                    if (results[i]._source.Email != undefined) emailData[x] = results[i]._source.Email;
                    if (results[i]._source["UI - Bulk Upload"] != undefined) bulkData[x] = results[i]._source["UI - Bulk Upload"];
                    if (results[i]._source["UI - Web Form"] != undefined) webData[x] = results[i]._source["UI - Web Form"];
                }
            }
        }

        this.setState({
            totalUploadEvents: {
                chart: { type: 'column', height: '250', width: '980', style: { fontFamily: 'Roboto,sans-serif' } },
                title: { text: 'VIOLATION UPLOAD EVENTS', align: 'left', style: { "fontSize": "18px" } },
                colors: ['#FEC000', '#F14061', '#BA1F30'],
                xAxis: { categories: xAxis, lineWidth: 0, minorGridLineWidth: 0, lineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0, labels: { enabled: false }, title: { text: null } },
                legend: { layout: 'vertical', align: 'left', x: -10, verticalAlign: 'top', y: 40, margin: 30, symbolRadius: 0, reversed: true, borderWidth: 0, symbolHeight: 15, itemMarginTop: 6, itemStyle: { "fontSize": "11px", "fontWeight": "normal" } },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' } },
                credits: { enabled: false },
                series: [{
                    name: 'Email Submission',
                    data: emailData
                }, {
                    name: 'Bulk Upload',
                    data: bulkData
                }, {
                    name: 'Manual Submission',
                    data: webData
                }]
            }
        });

    }

    render() {

        return (
            <div className="contact">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="contact-title">
                                <h1>Copyright Infringement Submission</h1>
                                <p>You have four options to submit copyright infringement notifications. You can submit an individual infringement via online form and/or by sending a standardized email, upload your infringement record file or initiate your system integration to automate your submission process in the future.</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="col-md-3 contact-blog"  onClick={()=>this.ManualSubmissions()}>
                                <div className="contact-box">
                                    <div className="contact-box-text">
                                        <h5>Fill Online Form</h5>
                                        <p>To submit and track individual infringement, please fill our online form</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 contact-blog" onClick={()=>this.sendEmail()}>
                                <div className="contact-box email">
                                    <div className="contact-box-text">
                                        <h5>Send an e-mail </h5>
                                        <p>To submit infringement via email, please fill the necessary information in the prepopulated email</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 contact-blog">
                                <div className="contact-box bulk">
                                    <div className="contact-box-text">
                                        <h5>Report in Bulk<br />(Upload file)</h5>
                                        <p>Bulk Uploads support Microsoft Excel and Google sheets. DMCA can sync with directory from <strong>Google Drive</strong>, <strong>DropBox</strong>, or <strong>Box</strong> </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 contact-blog">
                                <div className="contact-box integrate">
                                    <div className="contact-box-text">
                                        <h5>Integrate (Get SDK)</h5>
                                        <p>Integrate directly with your internal syatems and competely automate the process. please download our API SDK.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="upload-events">
                                <ReactHighcharts config={this.state.totalUploadEvents}></ReactHighcharts>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    const {dataReducer, dataAction} = state;
    return {dataReducer, dataAction}
}
export default connect(mapStateToProps)(Submission);
