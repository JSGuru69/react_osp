import React, {Component} from 'react';
import {connect} from 'react-redux';
import {dataAction} from '../redux/action'; // Redux Action
import Sidebar from '../components/NavMenu/SidebarDashboard'
const ReactHighcharts = require('react-highcharts');
import {Accordion, Panel} from 'react-bootstrap';
import Search from '../components/Search'
import _ from 'lodash'
import rules from '../../data/rules';
import moment from 'moment'
var classNames = require('classnames');
import Cb from './Subscriber/Checkbox'



class SearchResult extends Component {
    constructor(props) {
        super(props);
        ['click'].forEach((name) => this[name] = this[name].bind(this));
        this.prepareChart = this.prepareChart.bind(this);

        this.state = {
            activeIndex: -1,
            click: false,
            charts:{},
            violationHistoryConfig:{}
        }

    }

    componentDidMount() {
      fetch('/search_violation.json')
      .then(response => {
        console.log(response);
          return response.json();
      })
      .then(charts => {
        console.log(charts);
          this.setState({ charts });
          this.prepareChart(this.state.charts);
      });

      this.action('', 'users')

    }

    prepareChart(res){
      var results = res.violationhistory.hits.hits;
      let data = [];
      var vioDate;
      for (var item in results) {
          vioDate = new Date(results[item]._source.notification_date);
          var d = Date.UTC(vioDate.getFullYear(), vioDate.getMonth(), vioDate.getDate())
          let dataItem = [d, results[item]._source.infringement_level];
          console.log(dataItem);
          data.push(dataItem);
      }

      this.setState({
          violationHistoryConfig: {
          chart: { type: 'line', backgroundColor: '#494B62', width: '273', height: '288' },
          colors: ['#FFC31F'],
          title: { text: 'Violation History', align: 'left', style: { "color": "#fff" } },
          xAxis: { type: 'datetime', labels: { enabled: false } },
          yAxis: { min: 1, max: 8, tickInterval: 1, title: { text: null }, labels: { enabled: true, style: { color: '#fff' } }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0, tickLength: 0 },
          credits: { enabled: false },
          plotOptions: { series: { marker: { enabled: true } } },
          series: [{ data: data, showInLegend: false }]
      }
    });

    }

    onSearchSelect(val) {
        this.action(val,'saveSelectData')
    }

    click(val) {
        this.setState({
            click: val
        })
    }

    handlePanelClick(index, event) {
        let activeindex = -1;
        if (event.target.nodeName == "A") {
            if (event.target.className != "") {
                activeindex = index;
            }
        }
        else if (event.target.parentNode.className != "") {
            activeindex = index;
        }
        this.setState({activeIndex: activeindex});

    }

    action(params, actionItem) {
        const {dispatch} = this.props,
            actionList = {
                source: actionItem,
                params: params,
                actions: {
                    users: {
                        request: {
                            method:'GET',
                            url: 'users',
                            params: {
                                path: 'users'
                            }
                        },
                        success: {
                            triggers: ['titles']
                        },
                        targetPath: 'users'
                    },
                    titles: {
                        request: {
                            method:'GET',
                            url: 'titles',
                            params: {
                                path: 'titles'
                            }
                        },
                        targetPath: 'titles'
                    },
                    saveSelectData: {
                        value: params,
                        targetPath: 'searchSelect'
                    }
                }
            };
        dispatch(dataAction(actionList));
    }

    render() {

      console.log("violation history data");
      console.log(this.state.violationHistoryConfig);


        const {activeIndex} = this.state;
        const data = this.props.dataStore;

        let searchContainer,
            _this = this;
        if (!_.isEmpty(data.userDetail)) {
            searchContainer =
                <Search
                    data={this.props.dataStore}
                    onSelect={(val)=> this.onSearchSelect(val)}
                />
        }


        let gData = _.groupBy(data.userDetail.subscriberDetails.hits.hits, x => x._source.infringement_level),
            tData = data.titles;
        var btnClass = classNames({
            'active': this.state.click,
        });

        let userContainer,
            titleContainer;


        if (this.props.params.type == 'user') {
            userContainer =
                <div className="table-container">
                    <div className="col-md-4 name-container">
                        <div className="name">
                            {data.userDetail.name}
                            <br />
                            Member Since {moment(data.userDetail.member_since).format('ll')}
                        </div>
                        <div className="clearfix"></div>
                        <div className="detail">
                            {data.userDetail.id} <br />
                        </div>
                    </div>
                    <div className="col-md-4 name-container">
                        <div className="name">
                            WAN STATUS <span className={data.userDetail.status ? 'green' : 'red'}></span>
                        </div>
                        <div className="clearfix"></div>
                        <div className="detail">
                            WAN IP ADDRESS {data.userDetail.wan_ip}
                            <br/>
                            DNS Server
                        </div>
                        <div className="clearfix"></div>

                        <div className="name">
                            Devise Log
                            <br/>
                            <img src="/src/assets/images/log.png" alt=""/>
                        </div>
                    </div>
                    <div className="col-md-4 name-container">
                        <div className="name">
                            Violation History
                        </div>
                        <div className="clearfix"></div>
                        <div className="chart">
                          <ReactHighcharts config={this.state.violationHistoryConfig}></ReactHighcharts>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    <div className="col-md-12 tabs manage-rules">
                        <Accordion>
                            {
                                Object.keys(gData).map((item, index)=>
                                    <Panel className={index==activeIndex?"tab-open":"tab-close"} key={index} eventKey={index} header={
                                        <div className="header">
                                            LEVEL { gData[item][0]._source.infringement_level } : {gData[item][0]._source.infringements} Violations
                                            <i className="material-icons sunny">wb_sunny</i>
                                            <i className="material-icons arrow-down">{index == activeIndex ? "keyboard_arrow_down" : "keyboard_arrow_right"}</i>
                                            <span>Active</span>
                                        </div>
                                    } onClick={this.handlePanelClick.bind(this,index)}>
                                        <div className="panel-group inside-panel" id="accordion">

                                            {
                                                gData[item].map((val, i)=>
                                                    <div className="panel panel-default" id={`panelSub${val._source.hash_value}`}>
                                                        <a data-toggle="collapse" data-parent="#accordion" href={`#collapse${val._source.hash_value}`}><div className="panel-heading">
                                                            <div className="header">
                                                                { val._source.infringed_content }
                                                                <span>{ this.state.click == val._source.hash_value ? 'ACKNOWLEDGE' : 'OUTSTANDING' }</span>
                                                                <span className="date">{ moment(val._source.datestamp).format('LL') }</span>
                                                            </div>
                                                        </div></a>
                                                        <div id={`collapse${val._source.hash_value}`} className="panel-collapse collapse">
                                                            <div className="panel-body">

                                                                <div className="tab-open">
                                                                    <div className="wrapper">
                                                                        <div className="row">
                                                                            <div className="col-md-12 table-cont">
                                                                                <table className="table table-hover">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th><span>DATE OF VIOLATION</span></th>
                                                                                        <th><span>TYPE OF VIOLATION</span></th>
                                                                                        <th><span>DATE RECEIVED BY ISP</span></th>
                                                                                        <th><span>DECLARATION OF ACCURACY</span></th>
                                                                                        <th><span>SIGNED</span></th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>03/25/2017	</td>
                                                                                        <td>Transmission</td>
                                                                                        <td>03/26/2017	</td>
                                                                                        <td>CAR</td>
                                                                                        <td>CAR</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>

                                                                                <table className="table table-hover">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th><span>SERVICE USED</span></th>
                                                                                        <th><span>DEVICE NAME</span></th>
                                                                                        <th><span>DEVICE IP IN LAN</span></th>
                                                                                        <th><span>HASH VALUE</span></th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>Revision3</td>
                                                                                        <td>Android2309888822</td>
                                                                                        <td>192.168.216</td>
                                                                                        <td>ty3y21o078oo92d89778cf4z1oc8ov8</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table className="table">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>MAINTENANCE CONTOL</th>
                                                                                        <th></th>
                                                                                        <th></th>
                                                                                        <th>DISPUTE CONTROL</th>
                                                                                        <th></th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button onClick={()=>this.click(val._source.hash_value)} className={btnClass}>
                                                                                                ACKNOWLEDGE
                                                                                            </button>
                                                                                        </td>
                                                                                        <td>
                                                                                            <button>
                                                                                                RE SEND MAIL
                                                                                            </button>
                                                                                        </td>
                                                                                        <td>
                                                                                            <button>
                                                                                                SEND DISPUTE FORM
                                                                                            </button>
                                                                                        </td>
                                                                                        <td>
                                                                                            <select id=""
                                                                                                    className='form-control'
                                                                                                    onChange={e => this.setState({ selected: e.target.value || null })}
                                                                                                    value={this.state.selected || ''}
                                                                                            >
                                                                                                {
                                                                                                    ['Subscriber owns content', 'Inccorrect notifications', 'Other'].map(function(opt, a) {
                                                                                                        return (
                                                                                                            <option key={a} value={opt}>
                                                                                                                {opt}
                                                                                                            </option>
                                                                                                        )
                                                                                                    })
                                                                                                }
                                                                                            </select>
                                                                                        </td>
                                                                                        <td>
                                                                                            <select id=""
                                                                                                    className='form-control'
                                                                                                    onChange={e => this.setState({ selected: e.target.value || null })}
                                                                                                    value={this.state.selected || ''}
                                                                                            >
                                                                                                {
                                                                                                    _.range(val._source.infringed_level).map(function(opt, a) {
                                                                                                        return (
                                                                                                            <option key={a} value={opt}>
                                                                                                                {opt}
                                                                                                            </option>
                                                                                                        )
                                                                                                    })
                                                                                                }
                                                                                            </select>
                                                                                        </td>

                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div className="col-md-12" style={{
                                                                                display: 'flex',
                                                                                height: '30',
                                                                                alignItems: 'center',
                                                                                justifyContent: 'center',
                                                                                color: 'green'
                                                                            }}>
                                                                                {this.state.click == val._source.hash_value ? 'Thank you' : ''}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            }


                                        </div>
                                    </Panel>

                                )


                            }
                        </Accordion>
                    </div>
                </div>
        }

        if (this.props.params.type == 'title') {
            titleContainer =
                <div className="table-container">
                    <div className="col-md-4 name-container">
                        <div className="check-container">
                            <Cb /> Select All
                        </div>
                    </div>
                    <div className="col-md-4 text-right">
                        Sort result by:

                    </div>
                    <div className="col-md-4 text-right name-container">
                        <select id=""
                                className='form-control'
                                onChange={e => this.setState({ selected: e.target.value || null })}
                                value={this.state.selected || ''}
                        >
                            {
                                _.range(4).map(function(opt, a) {
                                    return (
                                        <option key={a} value={opt}>
                                            {opt}
                                        </option>
                                    )
                                })
                            }
                        </select>
                    </div>
                    <div className="clearfix"></div>
                    <div className="col-md-12 tabs manage-rules">
                        <Accordion>
                            {
                                tData && tData.map((item, index)=>
                                    <Panel className={index==activeIndex?"tab-open":"tab-close"} key={index} eventKey={index} header={
                                        <div className="header">
                                            huntercove.mp4, 03/25/2017, Stargazerfilms
                                            <i className="material-icons sunny">wb_sunny</i>
                                            <i className="material-icons arrow-down">{index == activeIndex ? "keyboard_arrow_down" : "keyboard_arrow_right"}</i>
                                            <span>Active</span>
                                        </div>
                                    } onClick={this.handlePanelClick.bind(this,index)}>
                                        <div className="tab-open">
                                            <div className="wrapper">
                                                <div className="row">
                                                    <div className="col-md-12 table-cont">
                                                        <table className="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th><span>VIALATOR'S NAME</span></th>
                                                                <th><span>TIME STAMP</span></th>
                                                                <th><span>DAYS IN VIOLATION / PREVIOUS VIOLATIONS</span></th>
                                                                <th><span>URL / IP ADDRESS</span></th>
                                                                <th><span>GEO LOCATION</span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>Mario Bianco</td>
                                                                <td>03/25/2017 10:15AM</td>
                                                                <td>7/2</td>
                                                                <td>https://www.youtube.com/watch?v=g5kJA0f</td>
                                                                <td>47.4925, 19.0513</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <table className="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th><span>CONTENT OWNER</span></th>
                                                                <th><span>FILE NAME</span></th>
                                                                <th><span>HASH VALUE</span></th>
                                                                <th><span>FILE LOCATION</span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>Stargazerfilms</td>
                                                                <td>huntercove.mp4</td>
                                                                <td>ty3y21o078oo92d89778cf4z1oc8ov8</td>
                                                                <td>Google Drive</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table className="table">

                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    <button style={{
                                                                        width: '100%'
                                                                    }}>
                                                                        ACKNOWLEDGE
                                                                    </button>
                                                                </td>
                                                                <td>
                                                                    <button style={{
                                                                        width: '100%'
                                                                    }}>
                                                                        RE SEND MAIL
                                                                    </button>
                                                                </td>
                                                                <td>
                                                                    <button style={{
                                                                        width: '100%'
                                                                    }}>
                                                                        SEND DISPUTE FORM
                                                                    </button>
                                                                </td>

                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </Panel>

                                )


                            }
                        </Accordion>
                    </div>
                </div>
        }

        return (
            <div className="dashboard">
                <Sidebar/>

                <div className="dashboard-content">
                    <div className="title">
                        Copyright Infringement Admin
                    </div>
                    {searchContainer}
                    {userContainer}
                    {titleContainer}

                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    const {dataStore} = state;
    return {dataStore}
}
export default connect(mapStateToProps)(SearchResult);
