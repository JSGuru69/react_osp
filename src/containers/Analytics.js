import React, {Component} from 'react';
import {connect} from 'react-redux';
import Sidebar from '../components/NavMenu/SidebarDashboard'

class Analytics extends Component {
  constructor(props) {
    super(props);
    [
    ].forEach((name) => this[name] = this[name].bind(this));

    this.state = {}
  }

  render() {

    return (
      <div className="dashboard">
          <Sidebar/>
          <div className="dashboard-content" style={{marginTop:"150px"}}>

              Under Construction...

          </div>
      </div>
    );
  }
}


function mapStateToProps(state) {
  const {dataStore, dataAction} = state;
  return {dataStore, dataAction}
}
export default connect(mapStateToProps)(Analytics);