// React pack
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import NavMenu from '../components/NavMenu/NavBar'

import '../assets/styles/components.scss';
import '../assets/js/components.js';
import Chart from '../components/Chart/_Component'
import Footer from '../components/Footer/Footer';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lang: navigator.language || navigator.languages[0]
        }
    }

    render() {
        return (

          <div>
            {this.props.pathname === "/dashboard2" && <div>111111<Chart /></div>}

            {this.props.pathname !== "/dashboard2" &&
              <div className="osp-app" lang="EN">
                <NavMenu data={this.props} />
                <section id="page_container" className="page-container">
                  {this.props.children}
                    <Footer/>
                </section>
              </div>
            }

          </div>

        );
    }
}

function mapStateToProps(state) { const { dataReducer} = state;  return {  dataReducer }}
export default connect(mapStateToProps)(App);