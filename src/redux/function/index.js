import is from 'is_js';
import _ from 'lodash';
import endpoint from '../constants'; // Constants

export default class Func {
    static isCallAction(args) {
        if (is.object(args) && !is.empty(args) && !args.actions[args.source]) {
            console.warn('----------------------------------------------------');
            console.warn('Make sure that the invoked parameters are in the object on the container component');
            console.warn('----------------------------------------------------');
            return false;
        }
        return true;
    }
    static isCallReducer(args) {
        if (!args.actions && _.isEmpty(args.actions) && _.isEmpty(args.source) && !_.isString(args.source)) {
            console.warn('----------------------------------------------------');
            console.warn('Action Throwing error');
            console.warn('----------------------------------------------------');
            return false;
        }
        return true;
    }
    static parameters(args) {
        return !is.empty(args) ? {
            type: 'dsl-action',
            source: Func.getSource(args),
            params: Func.getParams(args),
            actions: Func.actions(args),
            targetPath: Func.getTargetPath(args),
            func: Func.getFunc(args),
            request: Func.getRequest(args),
            success: Func.getSuccess(args),
            error: Func.getError(args),
            fetchStatus: null,
            value: Func.getVal(args),
            store: args.store
        } : null
    }
    static actions() {
        return arguments[0].actions && is.object(arguments[0].actions) && !is.empty(arguments[0].actions) ? arguments[0].actions : null;
    }
    static getSource() {
        return arguments[0].source && is.string(arguments[0].source) && !is.empty(arguments[0].source) ? arguments[0].source : null;
    }
    static getParams() {
        return arguments[0].params && !is.empty(arguments[0].params) ? arguments[0].params : null
    }
    static getTargetPath() {
        let actions = arguments[0].actions,
            source = arguments[1] ? arguments[1] : arguments[0].source;
        return actions[source].targetPath && is.string(actions[source].targetPath) && !is.empty(actions[source].targetPath) ? actions[source].targetPath : null;
    }
    static getFunc() {
        let actions = arguments[0].actions,
            source = arguments[1] ? arguments[1] : arguments[0].source;
        return is.object(actions[source]) && !is.empty(actions[source]) && is.function(actions[source].func) ? actions[source].func : null;
    }
    static getRequest() {
        let actions = arguments[0].actions,
            source = arguments[1] ? arguments[1] : arguments[0].source,
            item = actions[source],
            req = actions[source].request;

        if (actions && item && is.object(item) && !is.empty(item) && req && is.object(req)) {
            return {
                method: req.method && is.string(req.method) && !is.empty(req.method) ? req.method : null,
                url: !is.empty(req.url) && is.string(req.url) ? req.url : null,
                params: req.params && !is.empty(req.params) && is.object(req.params) ? req.params : null
            }
        }

        return null;
    }
    static getSuccess() {
        let actions = arguments[0].actions,
            source = arguments[1] ? arguments[1] : arguments[0].source,
            item = actions[source],
            success = item.success;

        if (actions && item && is.object(item) && !is.empty(item) && success && is.object(success)) {
            return {
                messages: !is.empty(success.messages) && is.array(success.messages) ? success.messages : null,
                triggers: !is.empty(success.triggers) && is.array(success.triggers) ? success.triggers : null,
                redirect: !is.empty(success.redirect) && is.string(success.redirect) ? success.redirect : null
            }
        }
        return null;
    }
    static getError() {
        let actions = arguments[0].actions,
            source = arguments[1] ? arguments[1] : arguments[0].source,
            item = actions[source],
            error = item.error;

        if (actions && item && is.object(item) && !is.empty(item) && error && is.object(error)) {
            return {
                messages: !is.empty(error.messages) && is.array(error.messages) ? error.messages : null,
                triggers: !is.empty(error.triggers) && is.array(error.triggers) ? error.triggers : null,
                redirect: !is.empty(error.redirect) && is.string(error.redirect) ? error.redirect : null
            }
        }
        return null;
    }
    static getVal() {

        let actions = arguments[0].actions,
            store = arguments[0].store.getState().dataStore,
            source = arguments[1] ? arguments[1] : arguments[0].source,
            val = actions[source].value ? actions[source].value : null;

        if (actions[source].value && is.not.empty(val)) {
            if (is.string(val)) {
                if (is.include(val, 'store:')) {
                    return Func.getData(val,store)
                } else {
                    return val
                }
            } else {
                return val
            }
        }
        return null;
    }
    static getFetchBody() {
        let bodyParams = arguments[0].request.params,
            method = arguments[0].request && is.object(arguments[0].request) && arguments[0].request.method && !is.empty(arguments[0].request.method) && is.string(arguments[0].request.method) ? arguments[0].request.method : null,
            body = bodyParams && is.object(bodyParams) && !is.empty(bodyParams) ? bodyParams : null,
            header = {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            fetchBody = {};

        if (body && method && (_.toUpper(method) == 'POST' || _.toUpper(method) == 'PUT') ) {
            fetchBody = {
                method: _.toUpper(method),
                body: JSON.stringify(body),
                headers: header
            }
        } else if(body && method && (_.toUpper(method) == 'DELETE' || _.toUpper(method) == 'GET')) {
            fetchBody = {
                method: _.toUpper(method),
                headers: header
            }
        }

        return fetchBody;
    }
    static fetchEndpoint() {

        let ep = arguments[0].request.url,
            params = arguments[0].request.params,
            store = arguments[0].store,
            endpointJson = {};

        Object.keys(params).forEach(function (t, i) {
            endpointJson = {
                ...endpointJson,
                [t]: Func.getData(params[t],store)
            }
        });

        return endpoint({type: ep, ...endpointJson});
    }
    static getData() {

        let data = arguments[0],
            getStoreData = arguments[1];

        if (data && !is.empty(data) && !is.number(data) && is.string(data) && _.includes(data, 'store:')) {
            let getVal = data.split(':')[1];
            return _.get(getStoreData, getVal)
        } else if (!is.empty(data) && !_.includes(data, 'store:')) {
            return data
        }
        return false
    }
    static parametersUpdate() {
        let parameters = arguments[0],
            val = arguments[1];

        return {
            ...Func.parameters(parameters),
            ...val
        }
    }
    static triggerFunc() {
        let parameters = arguments[0],
            type = arguments[1],
            dp = arguments[2],
            da = arguments[3],
            trigList = [];

        if (is.array(parameters[type].triggers) && !is.empty(parameters[type].triggers)) {
            parameters[type].triggers.forEach(function (t) {
                trigList.push(t);
            });
            Func.triggerAction(parameters,trigList,dp,da)
        }
    }
    static async triggerAction() {
        let parameters = arguments[0],
            trigList = arguments[1],
            dp = arguments[2],
            da = arguments[3];

        for (var i = 0; i < trigList.length; i++) {
            let item = trigList[i];
            let a = item.split(':');
            if (!a[1]) {
                a[1] = 0
            }
            await new Promise(resolve => setTimeout(() => {
                let source = a[0],
                    parametersUpdate = Func.parametersUpdate(parameters, {
                        type: 'dsl-action',
                        source: source,
                        params: Func.getParams(parameters),
                        targetPath: Func.getTargetPath(parameters),
                        func: Func.getFunc(parameters),
                        request: Func.getRequest(parameters,source),
                        success: Func.getSuccess(parameters),
                        error: Func.getError(parameters),
                        actions: Func.actions(parameters),
                        fetchStatus: null,
                        value: Func.getVal(parameters,source)
                    });
                if (source) {
                    dp(da(parametersUpdate))
                }
                return resolve(true)
            }, Number(a[1])));
        }
    }
    static getNoRequestParameter(type, parameters) {
        let messagesArray = parameters[type] && parameters[type].messages && is.array(parameters[type].messages) && !is.empty(parameters[type].messages) ? parameters[type].messages : [];
        return {
            params: type == 'success' ? parameters.params : null,
            fetchStatus: type,
            success: {
                ...parameters[type],
                messages : type == 'success' ? messagesArray : null
            },
            error: {
                ...parameters[type],
                messages : type == 'success' ? null : [{text:'Bilinmeyen bir hata'}]
            }
        }
    }
    static getRequestParameter(type, parameters, response) {
        let messagesArray = parameters[type] && parameters[type].messages && is.array(parameters[type].messages) && !is.empty(parameters[type].messages) ? parameters[type].messages : [];
        return {
            params: type == 'success' ? parameters.params : null,
            fetchStatus: type,
            value: type == 'success' ? response : null,
            success: {
                ...parameters[type],
                messages : type == 'success' ? messagesArray : null
            },
            error: {
                ...parameters[type],
                messages : type == 'success' ? null : [{text: response}]
            }
        }
    }
    //-------- Reducer
    static setFetchStatusData() {
        let parameters = arguments[0],
            state = arguments[1];

        switch (parameters.fetchStatus) {
            case 'success':
                state = Func.setProcess(state, parameters.source, 'success');
                Func.getMessage(parameters);
                return Func.setData(parameters.targetPath, parameters.value, state);
                break;
            case 'error':
                state = Func.setProcess(state, parameters.source, 'error');
                Func.getMessage(parameters);
                return Func.setData(parameters.targetPath, [], state);
                break;
            default:
                return Func.setData(parameters.targetPath, null, state);
        }
    }
    static setData() {
        let dataPath = arguments[0],
            updateData = arguments[1],
            getStoreData = arguments[2];
        if (!_.isEmpty(dataPath) && _.isString(dataPath)) {
            _.update(getStoreData, dataPath, function () {
                return updateData;
            });
            return getStoreData
        }
        return false
    }
    static message() {
        let msg = arguments[0],
            theme = arguments[1];

        console.log('-->',msg, theme)
    }
    static getMessage() {
        let params = arguments[0],
            messages = params[params.fetchStatus]
            && is.object(params[params.fetchStatus])
            && params[params.fetchStatus].messages
            && is.array(params[params.fetchStatus].messages)
            && !is.empty(params[params.fetchStatus].messages)
                ? params[params.fetchStatus].messages
                : null,
            val = params.fetchStatus == 'success' ? params.value : null,
            type = params.fetchStatus;

        if (messages) {
            messages.forEach(function (item, i) {

                let msg = item && is.object(item) && !is.empty(item) && item.text && !is.empty(item.text) && is.string(item.text) ? item.text : null,
                    sys = item && is.object(item) && !is.empty(item) && item.show && !is.empty(item.show) && is.string(item.show) ? item.show : 'on',
                    theme = item && is.object(item) && !is.empty(item) && item.theme && !is.empty(item.theme) && is.string(item.theme) ? item.theme : params.fetchStatus;

                if(msg && sys == 'off') {
                    console.warn('----------------------------------------------------');
                    console.warn('When the system message is "off", you can not write messages');
                    console.warn('Try showing a message using the function');
                    console.warn('----------------------------------------------------');
                    return false;
                }
                if (is.string(msg) && is.include(msg, '{{') && is.include(msg, '}}')) {
                    let msgPar = msg.split('{{')[1].split('}}')[0];
                    msg = msg.replace(/{{([\s\S]+?)}}/g, _.get(val,msgPar));
                }

                if (msg && sys == 'on') {
                    Func.message(msg, theme)
                }
            })
        }
    }
    static setProcess() {
        let state = arguments[0],
            source = arguments[1],
            type = {
                source: source,
                type: arguments[2],
                date: new Date()
            },
            index = _.findLastIndex(state.processBox, { 'source': source}),
            tempState;

        if (index > -1) {
            tempState = _.update(state, 'processBox['+ index +']', function () {
                return type;
            });
        } else {
            state.processBox.push(type);
            tempState = state;
        }
        return tempState;
    }
}