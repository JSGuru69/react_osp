export const initialState = {
    change_router: [], // Router change clean
    loading: true, // Loading
    processBox: [],
    //--------------------------
    lineData: [{
        name: 'Winter 2012-2013',
        data: [
            [Date.UTC(1970, 9, 21), 0],
            [Date.UTC(1970, 10, 4), 0.28],
            [Date.UTC(1970, 10, 9), 0.25],
            [Date.UTC(1970, 10, 27), 0.2],
            [Date.UTC(1970, 11, 2), 0.28],
            [Date.UTC(1970, 11, 26), 0.28],
            [Date.UTC(1970, 11, 29), 0.47],
            [Date.UTC(1971, 0, 11), 0.79],
            [Date.UTC(1971, 0, 26), 0.72],
            [Date.UTC(1971, 1, 3), 1.02],
            [Date.UTC(1971, 1, 11), 1.12],
            [Date.UTC(1971, 1, 25), 1.2],
            [Date.UTC(1971, 2, 11), 1.18],
            [Date.UTC(1971, 3, 11), 1.19],
            [Date.UTC(1971, 4, 1), 1.85],
            [Date.UTC(1971, 4, 5), 2.22],
            [Date.UTC(1971, 4, 19), 1.15],
            [Date.UTC(1971, 5, 3), 0]
        ]
    }, {
        name: 'Winter 2013-2014',
        data: [
            [Date.UTC(1970, 9, 29), 0],
            [Date.UTC(1970, 10, 9), 0.4],
            [Date.UTC(1970, 11, 1), 0.25],
            [Date.UTC(1971, 0, 1), 1.66],
            [Date.UTC(1971, 0, 10), 1.8],
            [Date.UTC(1971, 1, 19), 1.76],
            [Date.UTC(1971, 2, 25), 2.62],
            [Date.UTC(1971, 3, 19), 2.41],
            [Date.UTC(1971, 3, 30), 2.05],
            [Date.UTC(1971, 4, 14), 1.7],
            [Date.UTC(1971, 4, 24), 1.1],
            [Date.UTC(1971, 5, 10), 0]
        ]
    }, {
        name: 'Winter 2014-2015',
        data: [
            [Date.UTC(1970, 10, 25), 0],
            [Date.UTC(1970, 11, 6), 0.25],
            [Date.UTC(1970, 11, 20), 1.41],
            [Date.UTC(1970, 11, 25), 1.64],
            [Date.UTC(1971, 0, 4), 1.6],
            [Date.UTC(1971, 0, 17), 2.55],
            [Date.UTC(1971, 0, 24), 2.62],
            [Date.UTC(1971, 1, 4), 2.5],
            [Date.UTC(1971, 1, 14), 2.42],
            [Date.UTC(1971, 2, 6), 2.74],
            [Date.UTC(1971, 2, 14), 2.62],
            [Date.UTC(1971, 2, 24), 2.6],
            [Date.UTC(1971, 3, 2), 2.81],
            [Date.UTC(1971, 3, 12), 2.63],
            [Date.UTC(1971, 3, 28), 2.77],
            [Date.UTC(1971, 4, 5), 2.68],
            [Date.UTC(1971, 4, 10), 2.56],
            [Date.UTC(1971, 4, 15), 2.39],
            [Date.UTC(1971, 4, 20), 2.3],
            [Date.UTC(1971, 5, 5), 2],
            [Date.UTC(1971, 5, 10), 1.85],
            [Date.UTC(1971, 5, 15), 1.49],
            [Date.UTC(1971, 5, 23), 1.08]
        ]
    }],
    users: '',
    titles: '',
    detail: null,
    searchSelect: {},
    userDetail: {
        "id": "ISP-R-0004",
        "type": "Residential",
        "name": "Carol Ann Rockne",
        "address": "4678 Cardinal Dr. Beverly Hills,CA 90210",
        "email": "carolannrockne_dummy@isp.com",
        "wan_ip": "23.25.53.8",
        "member_since": "2016-01-01T00:00:00.000",
        "level": 7,
        "display_string": "Carol Ann Rockne\t\t\tLevel 7",
        "house_number": "4678",
        "street_name": "Cardinal Dr. Beverly Hills",
        "zipcode": "90210",
        "state": "CA",
        "dns_primary": "8.8.8.8",
        "dns_secondary": "9.9.9.9",
        "status": "suspended",
        "violationhistory": {
            "hits": {
                "hits": [
                    {
                        "_source": {
                            "infringement_level": 6,
                            "notification_date": "2016-08-04T01:55:24.000"
                        }
                    },
                    {
                        "_source": {
                            "infringement_level": 1,
                            "notification_date": "2016-08-04T05:55:24.000"
                        }
                    },
                    {
                        "_source": {
                            "infringement_level": 2,
                            "notification_date": "2016-08-11T05:55:24.000"
                        }
                    },
                    {
                        "_source": {
                            "infringement_level": 3,
                            "notification_date": "2017-02-03T18:35:09.000"
                        }
                    },
                    {
                        "_source": {
                            "infringement_level": 4,
                            "notification_date": "2017-02-07T09:40:39.000"
                        }
                    },
                    {
                        "_source": {
                            "infringement_level": 5,
                            "notification_date": "2017-02-18T12:58:04.000"
                        }
                    },
                    {
                        "_source": {
                            "infringement_level": 7,
                            "notification_date": "2017-02-28T13:36:03.000"
                        }
                    }
                ]
            }
        },
        "subscriberDetails": {
            "hits": {
                "hits": [
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 708,
                            "infringement_file_name": "AmericansS1:E2.mpg",
                            "datestamp": "2016-10-10T11:36:11.000",
                            "status": "Outstanding",
                            "subscriber_service_used": "Gnutella",
                            "abuse_date": "2016-10-10T11:36:11.000",
                            "hash_value": "c7rkdgdlhg435rqqsth5ieorjdpepomddmhipr",
                            "received_date": "2017-02-28T13:36:03.000",
                            "declared_by": "\"Jagannadha Paravastu (US)\"",
                            "signed_by": "\"Jagannadha Paravastu (US)\"",
                            "infringement_level": 7,
                            "infringements": 1,
                            "infringed_content": "AmericansS1:E2",
                            "infringement_icon": "local_movies"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 408,
                            "infringement_file_name": "Grey's Anatomy S1:E1.mov",
                            "datestamp": "2016-08-03T01:55:24.000",
                            "status": "Outstanding",
                            "subscriber_service_used": "Revision3",
                            "abuse_date": "2016-08-03T01:55:24.000",
                            "hash_value": "y7x5k92y2x764t879cjsi669xcpzm67",
                            "received_date": "2016-08-04T01:55:24.000",
                            "declared_by": "ABC",
                            "signed_by": "ABC",
                            "infringement_level": 6,
                            "infringements": 4,
                            "infringed_content": "Grey's Anatomy S1:E1",
                            "infringement_icon": "local_movies"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 410,
                            "infringement_file_name": "House of Cards S1:E1.qt",
                            "datestamp": "2016-08-03T01:55:24.000",
                            "status": "Outstanding",
                            "subscriber_service_used": "Gnutella",
                            "abuse_date": "2016-08-03T01:55:24.000",
                            "hash_value": "7bya0dzgn6w4z6m41j3k2q7p27334iu",
                            "received_date": "2016-08-04T01:55:24.000",
                            "declared_by": "Netflix",
                            "signed_by": "Netflix",
                            "infringement_level": 6,
                            "infringements": 4,
                            "infringed_content": "House of Cards S1:E1",
                            "infringement_icon": "album"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 181,
                            "infringement_file_name": "Narcos_span_sub S1:E6.qt",
                            "datestamp": "2017-01-30T05:12:39.000",
                            "status": "Acknowledged",
                            "subscriber_service_used": "Gnutella",
                            "abuse_date": "2017-01-30T05:12:39.000",
                            "hash_value": "24ba291f4a4f4gecb0ebe97fd626f028",
                            "received_date": "2017-02-20T17:19:12.000",
                            "declared_by": "\"Atul Rao (US - Advisory)\"",
                            "signed_by": "\"Atul Rao (US - Advisory)\"",
                            "infringement_level": 6,
                            "infringements": 4,
                            "infringed_content": "Narcos_span_sub S1:E6",
                            "infringement_icon": "album"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 409,
                            "infringement_file_name": "Grey's Anatomy S1:E1.mov",
                            "datestamp": "2016-08-03T01:55:24.000",
                            "status": "Acknowledged",
                            "subscriber_service_used": "Revision3",
                            "abuse_date": "2016-08-03T01:55:24.000",
                            "hash_value": "ty3y21o078oo92d89778cf4z1oc8ov8",
                            "received_date": "2016-08-04T01:55:24.000",
                            "declared_by": "ABC",
                            "signed_by": "ABC",
                            "infringement_level": 6,
                            "infringements": 4,
                            "infringed_content": "Grey's Anatomy S1:E1",
                            "infringement_icon": "local_movies"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 153,
                            "infringement_file_name": "Narcos_span_sub S1:E6.qt",
                            "datestamp": "2017-01-30T10:12:39.000",
                            "status": "Acknowledged",
                            "subscriber_service_used": "Gnutella",
                            "abuse_date": "2017-01-30T10:12:39.000",
                            "hash_value": "24ba291f4a4f4gecb0ebe97fd626f027",
                            "received_date": "2017-02-18T12:58:04.000",
                            "declared_by": "\"Atul Rao (US - Advisory)\"",
                            "signed_by": "\"Atul Rao (US - Advisory)\"",
                            "infringement_level": 5,
                            "infringements": 1,
                            "infringed_content": "Narcos_span_sub S1:E6",
                            "infringement_icon": "album"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 139,
                            "infringement_file_name": "Game of Thrones s1_e1.wav",
                            "datestamp": "2016-12-30T16:59:41.000",
                            "status": "Disputed",
                            "subscriber_service_used": "Mashboxx",
                            "abuse_date": "2016-12-30T16:59:41.000",
                            "hash_value": "38a8a6fg56a44d198518e333a3ae0fce",
                            "received_date": "2017-02-07T09:40:39.000",
                            "declared_by": "\"Amit Chaudhary (US - Advisory)\"",
                            "signed_by": "\"Amit Chaudhary (US - Advisory)\"",
                            "infringement_level": 4,
                            "infringements": 1,
                            "infringed_content": "Game of Thrones s1_e1",
                            "infringement_icon": "album"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 72,
                            "infringement_file_name": "House of Cards S1:E1.qt",
                            "datestamp": "2016-12-10T05:55:24.000",
                            "status": "Acknowledged",
                            "subscriber_service_used": "Gnutella",
                            "abuse_date": "2016-12-10T05:55:24.000",
                            "hash_value": "447kx5i8wn87572ysf7707gi54m89rf",
                            "received_date": "2017-02-03T18:35:09.000",
                            "declared_by": "\"Amit Chaudhary (US - Advisory)\"",
                            "signed_by": "\"Amit Chaudhary (US - Advisory)\"",
                            "infringement_level": 3,
                            "infringements": 1,
                            "infringed_content": "House of Cards S1:E1",
                            "infringement_icon": "album"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 53,
                            "infringement_file_name": "Homeland S1:E1.mpg",
                            "datestamp": "2016-08-10T05:55:24.000",
                            "status": "Disputed",
                            "subscriber_service_used": "Babelgum",
                            "abuse_date": "2016-08-10T05:55:24.000",
                            "hash_value": "8166jmf2p7405m3h8sgtdhc008p3mzi1",
                            "received_date": "2016-08-11T05:55:24.000",
                            "declared_by": "Showtime",
                            "signed_by": "Showtime",
                            "infringement_level": 2,
                            "infringements": 1,
                            "infringed_content": "Homeland S1:E1",
                            "infringement_icon": "local_movies"
                        }
                    },
                    {
                        "_source": {
                            "id": "ISP-R-0004",
                            "infringement_key": 28,
                            "infringement_file_name": "Homeland S1:E1.mpg",
                            "datestamp": "2016-08-03T05:55:24.000",
                            "status": "Acknowledged",
                            "subscriber_service_used": "Babelgum",
                            "abuse_date": "2016-08-03T05:55:24.000",
                            "hash_value": "8166jmf2p7405m3h8sgtdhc008p3mzi",
                            "received_date": "2016-08-04T05:55:24.000",
                            "declared_by": "Showtime",
                            "signed_by": "Showtime",
                            "infringement_level": 1,
                            "infringements": 1,
                            "infringed_content": "Homeland S1:E1",
                            "infringement_icon": "local_movies"
                        }
                    }
                ]
            }
        }
    }
};
