export default function endpoint(params) {

    let url = 'http://localhost:4500/',
        endpoint = {
            users:      url + params.path,
            titles:     url + params.path,
            detail:     url + 'user_detail'
        };

    return endpoint[params.type]
}
