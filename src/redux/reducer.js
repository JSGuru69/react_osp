
import React, {Component} from 'react'; // React
import {combineReducers} from 'redux';
import {initialState} from './constants/initialState';
import {routerReducer} from 'react-router-redux';
import Func from './function';
import is from 'is_js';
import _ from 'lodash';

export function dataReducer(state = initialState, action) {
    if (action.type) {
        if (_.startsWith(action.type, '@@router') && initialState.change_router && _.isArray(initialState.change_router) && !_.isEmpty(initialState.change_router) ) {
            function changeItemDataType() {
                let type;
                switch(arguments[0]) {
                    case Array:
                        type = [];
                        break;
                    case Object:
                        type = {};
                        break;
                    case Number || Boolean:
                        type = null;
                        break;
                    case String:
                        type = '';
                        break;
                    default:
                        type = 'undefined'
                }

                return type
            }
            let changeItems = initialState.change_router;
            changeItems.forEach(function (item, i) {
                _.update(state, item, function () {
                    return changeItemDataType(_.get(state, item).constructor);
                });
            });
        }

        if (action.type == 'API_REQUEST') {
            state = {...state, loading: true};
        }

        if (action.type == 'API_SUCCESS' || action.type == 'API_FAILURE') {
            state = {...state, loading: false};
        }

        if (!is.empty(action.actions) && !is.empty(action.source) && is.string(action.source)) {
            if (!Func.isCallReducer(action)) return;
            state = Func.setFetchStatusData(action ? action : null, state);
        }

    }

    state = {
        ...state
    };

    return state
}

export function device() {
    return {
        ie: is.ie() ? true : false,
        edge: is.edge() ? true : false,
        chrome: is.chrome() ? true : false,
        firefox: is.firefox() ? true : false,
        opera: is.opera() ? true : false,
        safari: is.safari() ? true : false,
        ios: is.ios() ? true : false,
        iphone: is.iphone() ? true : false,
        ipad: is.ipad() ? true : false,
        ipod: is.ipod() ? true : false,
        android: is.android() ? true : false,
        androidPhone: is.androidPhone() ? true : false,
        androidTablet: is.androidTablet() ? true : false,
        windowsPhone: is.windowsPhone() ? true : false,
        windowsTablet: is.windowsTablet() ? true : false,
        windows: is.windows() ? true : false,
        mac: is.mac() ? true : false,
        linux: is.linux() ? true : false,
        desktop: is.desktop() ? true : false,
        mobile: is.mobile() ? true : false,
        tablet: is.tablet() ? true : false,
        online: is.online() ? true : false,
        touchDevice: is.touchDevice() ? true : false
    }
}

const rootReducer = combineReducers({dataStore: dataReducer, routing: routerReducer, device: device });
export default rootReducer;