import {createStore, applyMiddleware, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import rest from './middleware/restMiddleware';
import DevTools from '../containers/DevTools';
import rootReducer from './reducer';

export default function configureStore(initialState) {

    const store = createStore(
        rootReducer,
        initialState, compose(
            applyMiddleware(
                thunkMiddleware,
                rest,
                createLogger()
            ), DevTools.instrument()
        )
    );

    if (module.hot) {
        module.hot.accept('./reducer', () => {
            const nextRootReducer = require('./reducer').default;
            store.replaceReducer(nextRootReducer);
        });
    }
    return store;
}

