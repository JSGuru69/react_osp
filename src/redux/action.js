import is from 'is_js';
import fetch from 'isomorphic-fetch'
import Func from './function';
import {browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import configureStore from './store';
const store = configureStore();
const history = syncHistoryWithStore(browserHistory, configureStore());

export const API_REQUEST = 'API_REQUEST';
export const API_SUCCESS = 'API_SUCCESS';
export const API_FAILURE = 'API_FAILURE';

export function dataAction(args) {

    args = {
        ...args,
        store: store
    };

    if (!Func.isCallAction(args)) return;
    let parameters = Func.parameters(args);
    function DP(params, result, dispatch) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                dispatch(dataCallReducer(params, result));
                if (parameters.func) {
                    parameters.func(result, parameters.fetchStatus)
                }
                if (parameters[parameters.fetchStatus].redirect && is.string(parameters[parameters.fetchStatus].redirect) && !is.empty(parameters[parameters.fetchStatus].redirect)) {
                    history.push(parameters[parameters.fetchStatus].redirect)
                }
                resolve();
            }, 100);
        });
    }
    function GT(params, dispatch) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                Func.triggerFunc(params, params.fetchStatus, dispatch, dataAction);
                resolve();
            }, 100);
        });
    }

    if (parameters.request && is.object(parameters.request) && parameters.request.url && parameters.request.method) {
        // console.log('EP--->', Func.fetchEndpoint(parameters.request));
        // console.log('POST--->', Func.getFetchBody(parameters.params, parameters.method));
        return {
            types: [API_REQUEST, API_SUCCESS, API_FAILURE],
            promise: fetch(
                Func.fetchEndpoint(parameters),
                Func.getFetchBody(parameters)
            ),
            success: (dispatch, response) => {
                parameters = Func.parametersUpdate(parameters, Func.getRequestParameter('success', parameters, response));
                DP(parameters, response, dispatch)
                    .then(() => GT(parameters, dispatch))
                    .catch(error => {
                        console.log(`Error : ${error}`)
                    });
            },
            error: (dispatch, response) => {
                parameters = Func.parametersUpdate(parameters, Func.getRequestParameter('error', parameters, response));
                DP(parameters, response, dispatch)
                    .then(() => GT(parameters, dispatch))
                    .catch(error => {
                        console.log(`Error : ${error}`)
                    });
            }
        }
    }
    else {
        return dispatch => {
            parameters = parameters && !parameters.request ? Func.parametersUpdate(parameters, Func.getNoRequestParameter('success', parameters)) : Func.parametersUpdate(parameters, Func.getNoRequestParameter('error', parameters))
            DP(parameters, parameters.value, dispatch)
                .then(() => GT(parameters, dispatch))
                .catch(error => {
                    console.log(`Error : ${error}`)
                });
        }
    }
}

export function dataCallReducer(parameters, response) {
    return {...parameters, value: response}
}