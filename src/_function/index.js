
export function isRequired(val) {
    return (
        val ? true : false
    )
}

export function minChar(val) {
    return (
        this.state.minChar && val.length && val.length < this.state.minChar ? true : false
    )
}

export function countCapitals(val) {
    return val.replace(/[^A-Z]/g, "").length > 0 ? true : false;
}

export function countNumbers(val) {
    return /\d/.test(val);
}

export function checkWords(val) {
    return _.some(this.state.forbiddenWords, function (word) {
        var matched = (word === val) ? true : "";
        return matched
    })
}

export function email(val) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(val);
}

export function alphanumeric(val) {
    var re = /[^a-zA-Z0-9\-\/]/;
    return re.test(val);
}

export function illegalChar(val) {
    var illegalChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?",
        k = 0;

    for (var i = 0; i < val.length; i++) {
        if (illegalChars.indexOf(val.charAt(i)) != -1){
            k++;
        }
    }

    return k > 0 ? false : true
}