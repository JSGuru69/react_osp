import React from 'react';
import { Router, Route, IndexRoute, Redirect } from 'react-router';

import App from '../containers/App';
import NotFound from '../components/404/Notfound';
import Dashboard from '../containers/Dashboard';
import ManageApis from '../containers/ManageApis';
import ManageRules from '../containers/ManageRules';

import Comp from '../containers/Comp';
import SearchResult from '../containers/SearchResult';
// import ManualSubmission from '../containers/ManualSubmission';
import Subscriber from '../containers/Subscriber';
import Chart from '../components/Chart/_Component'
import ogc_dashboard from '../components/ogc-dashbord//Index'

import Login from '../containers/Login';
import Landing from '../containers/Landing';
import Submission from '../containers/Submissions'
import ManualSubmission from '../containers/Submission/Manual';
import Analytics from '../containers/Analytics';
import OgcDashboard from '../components/Ogc/Index';

export default (

    <Router>
        <Route exact path="/subscriber" component={Subscriber}/>
        <Route exact path="/landing" component={Landing}/>

        <Route exact path="/" component={Login}/>
        <Route path="/" component={App}>
            {/*<Route path="dashboard" component={Dashboard}/>*/}
            <Route exact path="/submission" component={Submission}/>
            {/*<Route path="manual-submission" component={ManualSubmission}/>*/}
            <Route path="apis" component={ManageApis}/>
            <Route path="search-result/:type" component={SearchResult}/>
            <Route path="component-test" component={Comp}/>
            <Route path="rules" component={ManageRules}/>
            <Route path="dashboard2" component={Chart}/>
            <Route path="ogc-dashboard" component={ogc_dashboard}/>
            <Route path="dashboard" component={Chart}/>
            <Route path="ogc-dashboard" component={OgcDashboard}/>
            <Route path="analytics" component={Analytics}/>
            <Route path="submission/manual" component={ManualSubmission} />
            <Route path="*" component={NotFound}/>
        </Route>
    </Router>
);
