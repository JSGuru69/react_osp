import React, {Component} from 'react'; // React
import classNames from 'classnames';
import {browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import configureStore from '../../redux/store';
const history = syncHistoryWithStore(browserHistory, configureStore());

class Line extends Component {
    constructor(props) {
        super(props);
        [
            'search',
            'detail'
        ].forEach((name) => this[name] = this[name].bind(this));

        this.state = {}


    }

    componentDidMount() {

    }

    detail(val) {
        this.props.onSelect(val)
        history.push('/search-result/'+val.type)
    }

    search(e) {
        const {users, titles} = this.props.data;
        let val = _.trim(e.target.value),
            resultArray = [];
        if (val) {
            users.forEach(function (item, i) {
                if (_.includes(item._source.name.toLowerCase(), val.toLowerCase())) {
                    resultArray.push({
                        type: 'user',
                        data: item._source
                    })
                }
            });

            titles.forEach(function (item, i) {
                if (_.includes(item.file_name.toLowerCase(), val.toLowerCase())) {
                    resultArray.push({
                        type: 'title',
                        data: item
                    })
                }
            });

        } else {
            resultArray = []
        }

        this.setState({result: resultArray})
    }

    render() {

        let result,
            resultClass = classNames('result', {
                'show': this.state.result && this.state.result.length
            }),
            data;


        if (this.state.result && this.state.result.length) {

            result = this.state.result.map(function (item, i) {

                if (item.type == 'user') {
                    data =
                        <tr key={i} id={`scoreTable-${i}`} onClick={this.detail.bind(this, {type: 'user', data: item.data})}>
                            <td>
                                {item.data.name}
                            </td>
                            <td>
                                {item.data.address}
                            </td>
                            <td>
                                Level {item.data.level}
                            </td>
                        </tr>
                } else if (item.type == 'title') {
                    data =
                        <tr key={i} id={`scoreTable-${i}`} onClick={this.detail.bind(this, {type: 'title', data: item.data})}>
                            <td>
                                {item.data.file_name}
                            </td>
                            <td>
                            </td>
                            <td>

                            </td>
                        </tr>
                }
                return (
                    data
                );
            }, this)
        }

        return (
            <div className="search-form search-exp">
                <i className="material-icons">mic</i>
                <input type="text" onChange={(val) => this.search(val)}/>
                <i className="material-icons search-button">search</i>
                <div className={resultClass}>
                    <table className="table result-table" id="score_table">
                        <tbody id="score_table_body">
                        {result}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default Line
