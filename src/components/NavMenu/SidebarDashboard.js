import React, {Component} from 'react'; // React
import {Link} from 'react-router'
class SidebarManage extends Component {

    render() {

        return (
            <div className="sidebar">
                <ul>
                    <li>
                        <Link to="/dashboard" activeClassName="active">
                            <i className="material-icons">keyboard</i>
                            <span>Dashboard</span>
                        </Link>
                    </li>
                    <li>
                        <a>
                            <i className="material-icons">notifications</i>
                            <span>Notifications</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <i className="material-icons">question_answer</i>
                            <span>Q&A</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <i className="material-icons">insert_drive_file</i>
                            <span>Process</span>
                        </a>
                    </li>

                </ul>
            </div>
        )
    }
}

export default SidebarManage;

