import React, {Component} from 'react'; // React
import {Link} from 'react-router'

class NavMenu extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <nav className="navbar" id="navbar">
                <div className="nav-container">
                    <Link className="navbar-brand" to="/landing">
                        <div className="homeicon"></div>
                    </Link>
                    <ul className="nav nav-menu navbar-right">
                        <li className="dropdown">
                            <a href="#" className="dropdown-toggle md" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                            <span>
                                Welcome John Doe
                            </span>
                                <img src="/src/assets/images/usericon.png" alt=""/>
                                <div className="hamburger"></div>
                            </a>
                            <ul className="dropdown-menu small" role="menu">
                                <li><a href="#">Settings</a></li>
                                <li><a href="#">View/Edit Profile</a></li>
                                <li><a href="#">Display Density</a></li>
                                <li><a href="#">Help</a></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}

export default NavMenu;
