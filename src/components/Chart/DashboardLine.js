import React, {Component} from 'react'; // React
import Highcharts from 'highcharts'

class Line extends Component {
    constructor(props) {
        super(props);

    }

    graph() {
        var chart;
        if (chart) {chart.clear();}

        chart = Highcharts.chart('container', {
            chart: {
                type: 'spline',
                // Edit chart spacing
                spacingBottom: 10,
                spacingTop: 10,
                spacingLeft: 10,
                spacingRight: 10,
                height: 270,
                /*backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                    stops: [
                        [0, '#4A4B62'],
                        [1, '#4A4B62']
                    ]
                },*/
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: ''
                }
            },
            yAxis: {
                title: {
                    text: 'Snow depth (m)'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },

            series: this.props.data
        });

    }

    componentDidMount() {
        this.graph()

    }

    componentWillReceiveProps(nextProps) {
        this.graph(nextProps)
    }

    render() {


        return (
            <div>
                <div id="container"></div>
            </div>
        )
    }
}

export default Line
