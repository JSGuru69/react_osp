import React, {Component} from 'react'; // React
class SliderNav extends React.Component {

  componentDidMount(){
    $('.slidenav').click(function(){
      $('.slidenav').removeClass('nav-active'); 
      $(this).addClass('nav-active');
    });
  }

  bootChart(type){
  this.props.loadCharts(type);
  }

  render() {
    return (
      <div className="tab-menu">
          <ul>
              <li><a href="javascript:void(0)" className="slidenav nav-active" onClick={this.bootChart.bind(this, 'Y')} >Yearly</a></li>
              <li><a href="javascript:void(0)" className="slidenav" onClick={this.bootChart.bind(this, 'Q')} >Quarterly</a></li>
              <li><a href="javascript:void(0)" className="slidenav" onClick={this.bootChart.bind(this, 'M')} >Monthly</a></li>
              <li><a href="javascript:void(0)" className="slidenav" onClick={this.bootChart.bind(this, 'D')} >Daily</a></li>
          </ul>
      </div>
    );
  }
}


export default SliderNav
