import React, {Component} from "react";
import {connect} from 'react-redux';
import {dataAction} from '../../redux/action';
const ReactHighcharts = require('react-highcharts');
import {Link} from 'react-router';
// import Search from '../../components/Search';
import './_style.scss';
import Chart from './Chart'

import _ from 'lodash'

let jQuery = require('jquery');
require('flexslider');
let chartWidth: 361.20000000000005;

class Index extends Component {
    constructor(props) {
        super(props);
        this.prepareChart = this.prepareChart.bind(this);
        this.loadContentOwner = this.loadContentOwner.bind(this);
        this.loadNoticesChart = this.loadNoticesChart.bind(this);
        this.loadViolationAggregate = this.loadViolationAggregate.bind(this);
        this.loadServiceType = this.loadServiceType.bind(this);
        this.loadTrialTotalUploads = this.loadTrialTotalUploads.bind(this);
        this.loadTotalNotices = this.loadTotalNotices.bind(this);
        this.initSlider = this.initSlider.bind(this);
        this.loadCharts = this.loadCharts.bind(this);
        this.onSearchSelect = this.onSearchSelect.bind(this);


        this.state = {
            charts: {},
            serviceTypeYTD: {},
            serviceTypeQTD: {},
            serviceTypeMTD: {},
            serviceTypeDTD: {},
            totalNoticesYTD: {},
            totalNoticesYTD: {},
            totalNoticesQTD: {},
            totalNoticesMTD: {},
            totalNoticesChartYTD: {},
            totalNoticesChartDTD: {},
            totalNoticesChartMTD: {},
            totalNoticesChartQTD: {},
            totalUploadEventsYTD: {},
            totalUploadEventsQTD: {},
            totalUploadEventsMTD: {},
            totalUploadEventsDTD: {},
            violationYTD: {},
            violationQTD: {},
            violationMTD: {},
            violationDTD: {},
            contentOwnerYTD: {},
            contentOwnerQTD: {},
            contentOwnerMTD: {},
            contentOwnerDTD: {},

            //current Configurations
            contentTypeConfig: {},
            noticesConfig: {},
            violationConfig: {},
            methodOfViolationConfig: {},
            totalUploadsConfig: {},

            totalNotices: {},
            contentOwner: [],
        }
    }


  componentDidMount() {
    fetch('/db.json')
    .then(response => {
        return response.json();
    })
    .then(charts => {
      console.log(charts);
        this.setState({ charts });
        this.prepareChart(this.state.charts);
    });

    this.slider = $('.flexslider').flexslider({
      animation: "slide",
      controlNav: false,
      directionNav:false,
      animationLoop: false,
      slideshow: false
    });
  }

  initSlider(initPoint){
     this.slider.data('flexslider').flexAnimate(initPoint);

  }
  prepareChart(response){
    this.setState({
     serviceTypeYTD :JSON.parse(response.serviceTypeYTD),
     serviceTypeQTD : JSON.parse(response.serviceTypeQTD),
     serviceTypeMTD : JSON.parse(response.serviceTypeMTD),
     serviceTypeDTD : JSON.parse(response.serviceTypeDTD),
     totalNoticesYTD : JSON.parse(response.totalNoticesYTD),
     totalNoticesDTD : JSON.parse(response.totalNoticesDTD),
     totalNoticesQTD : JSON.parse(response.totalNoticesQTD),
     totalNoticesMTD : JSON.parse(response.totalNoticesMTD),
     totalNoticesChartYTD : JSON.parse(response.totalNoticesChartYTD),
     totalNoticesChartDTD : JSON.parse(response.totalNoticesChartDTD),
     totalNoticesChartMTD : JSON.parse(response.totalNoticesChartMTD),
     totalNoticesChartQTD : JSON.parse(response.totalNoticesChartQTD),
     totalUploadEventsYTD : JSON.parse(response.totalUploadEventsYTD),
     totalUploadEventsQTD : JSON.parse(response.totalUploadEventsQTD),
     totalUploadEventsMTD : JSON.parse(response.totalUploadEventsMTD),
     totalUploadEventsDTD : JSON.parse(response.totalUploadEventsDTD),
     violationYTD : JSON.parse(response.violationYTD),
     violationQTD : JSON.parse(response.violationQTD),
     violationMTD : JSON.parse(response.violationMTD),
     violationDTD : JSON.parse(response.violationDTD),
     contentOwnerYTD : JSON.parse(response.contentOwnerYTD),
     contentOwnerQTD : JSON.parse(response.contentOwnerQTD),
     contentOwnerMTD : JSON.parse(response.contentOwnerMTD),
     contentOwnerDTD : JSON.parse(response.contentOwnerDTD)
  });
     this.loadCharts('Y');

  }

  loadCharts(type){
    console.log(type);
    switch (type) {
      case "Y":
          this.initSlider(3);
          if (this.state.serviceTypeYTD.hasOwnProperty("hits")) this.loadServiceType(this.state.serviceTypeYTD.hits.hits, type);
          if (this.state.totalNoticesYTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesYTD.hits.hits, type);
          if (this.state.totalNoticesChartYTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartYTD.hits.hits, type);
          if (this.state.contentOwnerYTD.hasOwnProperty("hits")) this.loadContentOwner(this.state.contentOwnerYTD.hits.hits, type);
          if (this.state.totalUploadEventsYTD.hasOwnProperty("hits")) this.loadTrialTotalUploads(this.state.totalUploadEventsYTD.hits.hits, type);
          if (this.state.violationYTD.hasOwnProperty("hits")) this.loadViolationAggregate(this.state.violationYTD.hits.hits, type);
          break;
      case "Q":
          this.initSlider(2);
          if (this.state.serviceTypeQTD.hasOwnProperty("hits")) this.loadServiceType(this.state.serviceTypeQTD.hits.hits, type);
          if (this.state.totalNoticesQTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesQTD.hits.hits, type);
          if (this.state.totalNoticesChartQTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartQTD.hits.hits, type);
          if (this.state.contentOwnerYTD.hasOwnProperty("hits")) this.loadContentOwner(this.state.contentOwnerYTD.hits.hits, type);
          if (this.state.totalUploadEventsQTD.hasOwnProperty("hits")) this.loadTrialTotalUploads(this.state.totalUploadEventsQTD.hits.hits, type);
          if (this.state.violationQTD.hasOwnProperty("hits")) this.loadViolationAggregate(this.state.violationQTD.hits.hits, type);
          break;
      case "M":
          this.initSlider(1);
          if (this.state.serviceTypeMTD.hasOwnProperty("hits")) this.loadServiceType(this.state.serviceTypeMTD.hits.hits, type);
          if (this.state.totalNoticesMTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesMTD.hits.hits, type);
          if (this.state.totalNoticesChartMTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartMTD.hits.hits, type);
          if (this.state.contentOwnerMTD.hasOwnProperty("hits")) this.loadContentOwner(this.state.contentOwnerMTD.hits.hits, type);
          if (this.state.totalUploadEventsMTD.hasOwnProperty("hits")) this.loadTrialTotalUploads(this.state.totalUploadEventsMTD.hits.hits, type);
          if (this.state.violationMTD.hasOwnProperty("hits")) this.loadViolationAggregate(this.state.violationMTD.hits.hits, type);
          break;

      case "D":
          this.initSlider(0);
          if (this.state.serviceTypeDTD.hasOwnProperty("hits")) this.loadServiceType(this.state.serviceTypeDTD.hits.hits, type);
          if (this.state.totalNoticesDTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesDTD.hits.hits, type);
          if (this.state.totalNoticesChartDTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartDTD.hits.hits, type);
          if (this.state.contentOwnerDTD.hasOwnProperty("hits")) this.loadContentOwner(this.state.contentOwnerDTD.hits.hits, type);
          if (this.state.totalUploadEventsDTD.hasOwnProperty("hits")) this.loadTrialTotalUploads(this.state.totalUploadEventsDTD.hits.hits, type);
          if (this.state.violationDTD.hasOwnProperty("hits")) this.loadViolationAggregate(this.state.violationDTD.hits.hits, type);
          break;

      default:

    };

  }

  loadContentOwner(results, type) {
      this.setState({ contentOwner:results });

      //settinf Configuration State for the content Type chart
        this.setState({
           contentTypeConfig:{
                 chart: { type: 'column', height: '157.5', width: chartWidth, style: { fontFamily: 'Roboto,sans-serif', outline: '1px solid #D8D8D8', marginBottom: '7px' }, inverted: true },
                 title: { text: 'CONTENT TYPE', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                 colors: ['#BCBDBD', '#757575', '#FE5916', '#F8BACF', '#EA1C62', '#C2165B'],
                 xAxis: { categories: ['Content Type'], labels: { enabled: false }, title: { text: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                 yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                 legend: { enabled: true, layout: 'vertical', align: 'left', verticalAlign: 'middle', itemStyle: { "fontSize": "11px", "fontWeight": "normal", lineHeight: "15px" }, symbolHeight: 15, borderWidth: 0, symbolRadius: 0, itemMarginTop: 5, y: 10, reversed: true, lineHeight: 15 },
                 plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                 credits: { enabled: false },
                 series: [{ name: 'Image', data: [9], pointWidth: 50 }, { name: 'Apps', data: [5], pointWidth: 50 }, { name: 'Games', data: [11], pointWidth: 50 }, { name: 'TV', data: [7], pointWidth: 50 }, { name: 'Music', data: [15], pointWidth: 50 }, { name: 'Movie', data: [21], pointWidth: 50 }]
           }
         });

      }

      loadNoticesChart(results, type) {
          let xAxis = [];
          for (var i in results) {
              if (xAxis.indexOf(results[i]._source.time_series_value) == -1)
                  xAxis.push(results[i]._source.time_series_value)
          }

          let forecastData = [];
          let actualData = [];
          for (var item in results) {
              // let dataItem = [results[item]._source.infringement_level];
              // console.log(dataItem);
              forecastData.push(results[item]._source.forecast_notices);
              actualData.push(results[item]._source.actual_notices);
          }


            this.setState({
               noticesConfig:{
                  chart: { type: 'spline', backgroundColor: '#FFFFFF', width: '445', height: '400' },
                  colors: ['#DE7427', '#A62024'],
                  title: { text: '', align: 'left', style: { "color": "#fff" } },
                  xAxis: { labels: { enabled: false }, tickWidth: 0 },
                  yAxis: { title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#D8D8D8', minorTickLength: 0, tickLength: 0 },
                  credits: { enabled: false },
                  tooltip:{headerFormat: '',},
                  plotOptions: { series: { marker: { enabled: true } } },
                  series: [{ data: forecastData, showInLegend: false, name: "Forecast",lineWidth: 6 }, { data: actualData, showInLegend: false, name: "Actual",lineWidth: 6 }]
                  }
                });
      }

      loadViolationAggregate(results, type) {
          let level8Total = 0;
          let level1Total = 0;
          let level2Total = 0;
          let level3Total = 0;
          let level4Total = 0;
          let level5Total = 0;
          let level6Total = 0;
          let level7Total = 0;
          for (var i in results) {
              switch (results[i]._source.infringement_level_name) {
                  case "Level 1":
                      level1Total = results[i]._source.total_notices;
                      break;
                  case "Level 2":
                      level2Total = results[i]._source.total_notices;
                      break;
                  case "Level 3":
                      level3Total = results[i]._source.total_notices;
                      break;
                  case "Level 4":
                      level4Total = results[i]._source.total_notices;
                      break;
                  case "Level 5":
                      level5Total = results[i]._source.total_notices;
                      break;
                  case "Level 6":
                      level6Total = results[i]._source.total_notices;
                      break;
                  case "Level 7":
                      level7Total = results[i]._source.total_notices;
                      break;
                  case "Level 8":
                      level8Total = results[i]._source.total_notices;
                      break;
              }
          }

            this.setState({
              violationConfig:{
                  chart: { type: 'column', height: '350', width: '120', style: { "fontFamily": "Roboto,sans-serif" }, marginBottom: 35 },
                  title: { text: 'VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                  colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#BF4149', '#89091F'],
                  xAxis: { categories: ['Violations'], labels: { enabled: false }, title: { text: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                  yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                  legend: { enabled: false },
                  plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                  credits: { enabled: false },
                  series: [{
                      name: 'Level 8',
                      data: [level8Total]
                  }, {
                      name: 'Level 7',
                      data: [level7Total]
                  }, {
                      name: 'Level 6',
                      data: [level6Total]
                  }, {
                      name: 'Level 5',
                      data: [level5Total]
                  }, {
                      name: 'Level 4',
                      data: [level4Total]
                  }, {
                      name: 'Level 3',
                      data: [level3Total]
                  }, {
                      name: 'Level 2',
                      data: [level2Total]
                  }, {
                      name: 'Level 1',
                      data: [level1Total]
                  }]
                }
                  });
      }

      loadServiceType(results, type) {
          let eventArr = [];
          for (var i in results) {
              eventArr.push({
                  name: results[i]._source.subscriber_service_used,
                  data: [results[i]._source.total_notices],
                  type: 'column',
                  pointWidth: 100
              })
          }
          this.setState({
            methodOfViolationConfig:{
                chart: { type: 'column', height: '157.5', width: chartWidth, style: { fontFamily: 'Roboto,sans-serif', 'outline': '1px solid #D8D8D8' }, inverted: true },
                title: { text: 'METHOD OF VIOLATION', align: 'left', style: { "fontSize": "15px",  "color": "#000", "fontWeight": "bold" } },
                colors: ['#FE3E81', '#E1BCE7', '#BCBDBD', '#757575', '#FE5916', '#F8BACF', '#EA1C62', '#C2165B', '#9D18B2', '#7D0FA4'],
                xAxis: { categories: ['Service Type'], labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: true, itemStyle: { "fontSize": "11px", "fontWeight": "normal" }, symbolHeight: 15, borderWidth: 0, symbolRadius: 0, reversed: true, itemMarginTop: 5 },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: eventArr
            }
          });
      }

      loadTrialTotalUploads(results, type) {
          let xAxis = [];
          //Get the x axis Array
          for (var i in results) {
              if (xAxis.indexOf(results[i]._source.time_series_value) == -1)
                  xAxis.push(results[i]._source.time_series_value)
          }
          let level8Data = new Array(xAxis.length).fill(0);
          let level1Data = new Array(xAxis.length).fill(0);
          let level2Data = new Array(xAxis.length).fill(0);
          let level3Data = new Array(xAxis.length).fill(0);
          let level4Data = new Array(xAxis.length).fill(0);
          let level5Data = new Array(xAxis.length).fill(0);
          let level6Data = new Array(xAxis.length).fill(0);
          let level7Data = new Array(xAxis.length).fill(0);
          //Now get the
          for (var i in results) {
              for (var x in xAxis) {
                  if (xAxis[x] == results[i]._source.time_series_value) {
                      if (results[i]._source.infringement_level_name == "Level 8") {
                          level8Data[x] = results[i]._source.total_notices;
                      }
                      if (results[i]._source.infringement_level_name == "Level 1") {
                          level1Data[x] = results[i]._source.total_notices;
                      }
                      if (results[i]._source.infringement_level_name == "Level 2") {
                          level2Data[x] = results[i]._source.total_notices;
                      }
                      if (results[i]._source.infringement_level_name == "Level 3") {
                          level3Data[x] = results[i]._source.total_notices;
                      }
                      if (results[i]._source.infringement_level_name == "Level 4") {
                          level4Data[x] = results[i]._source.total_notices;
                      }
                      if (results[i]._source.infringement_level_name == "Level 5") {
                          level5Data[x] = results[i]._source.total_notices;
                      }
                      if (results[i]._source.infringement_level_name == "Level 6") {
                          level6Data[x] = results[i]._source.total_notices;
                      }
                      if (results[i]._source.infringement_level_name == "Level 7") {
                          level7Data[x] = results[i]._source.total_notices;
                      }
                  }
              }
          }

            this.setState({
              totalUploadsConfig:{
                  chart: { type: 'column', height: '350', width: 290, style: { "fontFamily": "Roboto,sans-serif" } },
                  title: { text: 'NOTICE TOTAL UPLOAD EVENTS', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                  colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#BF4149', '#89091F'],
                  xAxis: { categories: xAxis, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                  yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                  legend: { enabled: false },
                  plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                  credits: { enabled: false },
                  series: [{ name: 'Level 8', data: level8Data }, { name: 'Level 7', data: level7Data }, { name: 'Level 6', data: level6Data }, { name: 'Level 5', data: level5Data }, { name: 'Level 4', data: level4Data }, { name: 'Level 3', data: level3Data }, { name: 'Level 2', data: level2Data }, { name: 'Level 1', data: level1Data }]
              }
            });
      }

      loadTotalNotices(results, type) {
          console.log("Total notices: ", results);
          this.setState({ totalNotices:results[0]._source });

      }

  render() {
    return (
        <Chart
           contentOwner={this.state.contentOwner}
           totalNotices={this.state.totalNotices}
           noticesConfig={this.state.noticesConfig}
           contentTypeConfig={this.state.contentTypeConfig}
           methodOfViolationConfig={this.state.methodOfViolationConfig}
           violationConfig={this.state.violationConfig}
           totalUploadsConfig={this.state.totalUploadsConfig}
           loadCharts={this.loadCharts}
           />
    );
  }


    componentDidMount() {
        fetch('/db.json')
            .then(response => {
                return response.json();
            })
            .then(charts => {
                console.log(charts);
                this.setState({charts});
                this.prepareChart(this.state.charts);
            });

        this.slider = $('.flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav: false,
            animationLoop: false,
            slideshow: false
        });

        this.action('','users')

    }

    onSearchSelect(val) {
        this.action(val,'saveSelectData')
    }

    action(params, actionItem) {
        const {dispatch} = this.props,
            actionList = {
                source: actionItem,
                params: params,
                actions: {
                    users: {
                        request: {
                            method:'GET',
                            url: 'users',
                            params: {
                                path: 'users'
                            }
                        },
                        success: {
                            triggers: ['titles']
                        },
                        targetPath: 'users'
                    },
                    titles: {
                        request: {
                            method:'GET',
                            url: 'titles',
                            params: {
                                path: 'titles'
                            }
                        },
                        targetPath: 'titles'
                    },
                    saveSelectData: {
                        value: params,
                        targetPath: 'searchSelect'
                    }
                }
            };
        dispatch(dataAction(actionList));
    }

    initSlider(initPoint) {
        var currentSlide = this.slider.data('flexslider').currentSlide;
        console.log(initPoint);
        console.log(currentSlide);
        if (currentSlide != initPoint) {
            this.slider.data('flexslider').flexAnimate(initPoint);
        }

    }

    prepareChart(response) {
        this.setState({
            serviceTypeYTD: JSON.parse(response.serviceTypeYTD),
            serviceTypeQTD: JSON.parse(response.serviceTypeQTD),
            serviceTypeMTD: JSON.parse(response.serviceTypeMTD),
            serviceTypeDTD: JSON.parse(response.serviceTypeDTD),
            totalNoticesYTD: JSON.parse(response.totalNoticesYTD),
            totalNoticesDTD: JSON.parse(response.totalNoticesDTD),
            totalNoticesQTD: JSON.parse(response.totalNoticesQTD),
            totalNoticesMTD: JSON.parse(response.totalNoticesMTD),
            totalNoticesChartYTD: JSON.parse(response.totalNoticesChartYTD),
            totalNoticesChartDTD: JSON.parse(response.totalNoticesChartDTD),
            totalNoticesChartMTD: JSON.parse(response.totalNoticesChartMTD),
            totalNoticesChartQTD: JSON.parse(response.totalNoticesChartQTD),
            totalUploadEventsYTD: JSON.parse(response.totalUploadEventsYTD),
            totalUploadEventsQTD: JSON.parse(response.totalUploadEventsQTD),
            totalUploadEventsMTD: JSON.parse(response.totalUploadEventsMTD),
            totalUploadEventsDTD: JSON.parse(response.totalUploadEventsDTD),
            violationYTD: JSON.parse(response.violationYTD),
            violationQTD: JSON.parse(response.violationQTD),
            violationMTD: JSON.parse(response.violationMTD),
            violationDTD: JSON.parse(response.violationDTD),
            contentOwnerYTD: JSON.parse(response.contentOwnerYTD),
            contentOwnerQTD: JSON.parse(response.contentOwnerQTD),
            contentOwnerMTD: JSON.parse(response.contentOwnerMTD),
            contentOwnerDTD: JSON.parse(response.contentOwnerDTD)
        });
        this.loadCharts('Y');

    }

    loadCharts(type) {
        console.log(type);
        switch (type) {
            case "Y":
                this.initSlider(0);
                if (this.state.serviceTypeYTD.hasOwnProperty("hits")) this.loadServiceType(this.state.serviceTypeYTD.hits.hits, type);
                if (this.state.totalNoticesYTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesYTD.hits.hits, type);
                if (this.state.totalNoticesChartYTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartYTD.hits.hits, type);
                if (this.state.contentOwnerYTD.hasOwnProperty("hits")) this.loadContentOwner(this.state.contentOwnerYTD.hits.hits, type);
                if (this.state.totalUploadEventsYTD.hasOwnProperty("hits")) this.loadTrialTotalUploads(this.state.totalUploadEventsYTD.hits.hits, type);
                if (this.state.violationYTD.hasOwnProperty("hits")) this.loadViolationAggregate(this.state.violationYTD.hits.hits, type);
                break;
            case "Q":
                this.initSlider(1);
                if (this.state.serviceTypeQTD.hasOwnProperty("hits")) this.loadServiceType(this.state.serviceTypeQTD.hits.hits, type);
                if (this.state.totalNoticesQTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesQTD.hits.hits, type);
                if (this.state.totalNoticesChartQTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartQTD.hits.hits, type);
                if (this.state.contentOwnerYTD.hasOwnProperty("hits")) this.loadContentOwner(this.state.contentOwnerYTD.hits.hits, type);
                if (this.state.totalUploadEventsQTD.hasOwnProperty("hits")) this.loadTrialTotalUploads(this.state.totalUploadEventsQTD.hits.hits, type);
                if (this.state.violationQTD.hasOwnProperty("hits")) this.loadViolationAggregate(this.state.violationQTD.hits.hits, type);
                break;
            case "M":
                this.initSlider(2);
                if (this.state.serviceTypeMTD.hasOwnProperty("hits")) this.loadServiceType(this.state.serviceTypeMTD.hits.hits, type);
                if (this.state.totalNoticesMTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesMTD.hits.hits, type);
                if (this.state.totalNoticesChartMTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartMTD.hits.hits, type);
                if (this.state.contentOwnerMTD.hasOwnProperty("hits")) this.loadContentOwner(this.state.contentOwnerMTD.hits.hits, type);
                if (this.state.totalUploadEventsMTD.hasOwnProperty("hits")) this.loadTrialTotalUploads(this.state.totalUploadEventsMTD.hits.hits, type);
                if (this.state.violationMTD.hasOwnProperty("hits")) this.loadViolationAggregate(this.state.violationMTD.hits.hits, type);
                break;

            case "D":
                this.initSlider(3);
                if (this.state.serviceTypeDTD.hasOwnProperty("hits")) this.loadServiceType(this.state.serviceTypeDTD.hits.hits, type);
                if (this.state.totalNoticesDTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesDTD.hits.hits, type);
                if (this.state.totalNoticesChartDTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartDTD.hits.hits, type);
                if (this.state.contentOwnerDTD.hasOwnProperty("hits")) this.loadContentOwner(this.state.contentOwnerDTD.hits.hits, type);
                if (this.state.totalUploadEventsDTD.hasOwnProperty("hits")) this.loadTrialTotalUploads(this.state.totalUploadEventsDTD.hits.hits, type);
                if (this.state.violationDTD.hasOwnProperty("hits")) this.loadViolationAggregate(this.state.violationDTD.hits.hits, type);
                break;

            default:

        }
        ;

    }

    loadContentOwner(results, type) {
        this.setState({contentOwner: results});

        //settinf Configuration State for the content Type chart
        this.setState({
            contentTypeConfig: {
                chart: {
                    type: 'column',
                    height: '157.5',
                    width: chartWidth,
                    style: {fontFamily: 'Roboto,sans-serif', outline: '1px solid #D8D8D8', marginBottom: '7px'},
                    inverted: true
                },
                title: {
                    text: 'CONTENT TYPE',
                    align: 'left',
                    style: {"fontSize": "15px", "color": "#000", "fontWeight": "bold"}
                },
                colors: ['#B0B3B5', '#968B6D', '#e47100', '#FFB600', '#E0301E', '#A32020'],
                xAxis: {
                    categories: ['Content Type'],
                    labels: {enabled: false},
                    title: {text: false},
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                yAxis: {
                    min: 0,
                    title: {text: null},
                    labels: {enabled: false},
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                legend: {
                    enabled: true,
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'middle',
                    itemStyle: {"fontSize": "11px", "fontWeight": "normal", lineHeight: "15px"},
                    symbolHeight: 15,
                    borderWidth: 0,
                    symbolRadius: 0,
                    itemMarginTop: 5,
                    y: 10,
                    reversed: true,
                    lineHeight: 15
                },
                plotOptions: {
                    animation: false,
                    colorByPoint: true,
                    column: {stacking: 'normal'},
                    series: {animation: false}
                },
                credits: {enabled: false},
                series: [{name: 'Image', data: [9], pointWidth: 50}, {
                    name: 'Apps',
                    data: [5],
                    pointWidth: 50
                }, {name: 'Games', data: [11], pointWidth: 50}, {name: 'TV', data: [7], pointWidth: 50}, {
                    name: 'Music',
                    data: [15],
                    pointWidth: 50
                }, {name: 'Movie', data: [21], pointWidth: 50}]
            }
        });

    }

    loadNoticesChart(results, type) {
        let xAxis = [];
        for (var i in results) {
            if (xAxis.indexOf(results[i]._source.time_series_value) == -1)
                xAxis.push(results[i]._source.time_series_value)
        }

        let forecastData = [];
        let actualData = [];
        for (var item in results) {
            // let dataItem = [results[item]._source.infringement_level];
            // console.log(dataItem);
            forecastData.push(results[item]._source.forecast_notices);
            actualData.push(results[item]._source.actual_notices);
        }


        this.setState({
            noticesConfig: {
                chart: {type: 'spline', backgroundColor: '#FFFFFF', width: '445', height: '400'},
                colors: ['#DE7427', '#A62024'],
                title: {text: '', align: 'left', style: {"color": "#fff"}},
                xAxis: {labels: {enabled: false}, tickWidth: 0},
                yAxis: {
                    title: {text: null},
                    labels: {enabled: false},
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: '#D8D8D8',
                    minorTickLength: 0,
                    tickLength: 0
                },
                credits: {enabled: false},
                tooltip: {headerFormat: '',},
                plotOptions: {series: {marker: {enabled: true}}},
                series: [{data: forecastData, showInLegend: false, name: "Forecast", lineWidth: 6}, {
                    data: actualData,
                    showInLegend: false,
                    name: "Actual",
                    lineWidth: 6
                }]
            }
        });
    }

    loadViolationAggregate(results, type) {
        let level8Total = 0;
        let level1Total = 0;
        let level2Total = 0;
        let level3Total = 0;
        let level4Total = 0;
        let level5Total = 0;
        let level6Total = 0;
        let level7Total = 0;
        for (var i in results) {
            switch (results[i]._source.infringement_level_name) {
                case "Level 1":
                    level1Total = results[i]._source.total_notices;
                    break;
                case "Level 2":
                    level2Total = results[i]._source.total_notices;
                    break;
                case "Level 3":
                    level3Total = results[i]._source.total_notices;
                    break;
                case "Level 4":
                    level4Total = results[i]._source.total_notices;
                    break;
                case "Level 5":
                    level5Total = results[i]._source.total_notices;
                    break;
                case "Level 6":
                    level6Total = results[i]._source.total_notices;
                    break;
                case "Level 7":
                    level7Total = results[i]._source.total_notices;
                    break;
                case "Level 8":
                    level8Total = results[i]._source.total_notices;
                    break;
            }
        }

        this.setState({
            violationConfig: {
                chart: {
                    type: 'column',
                    height: '350',
                    width: '120',
                    style: {"fontFamily": "Roboto,sans-serif"},
                    marginBottom: 70
                },
                title: {
                    text: 'VIOLATION',
                    align: 'left',
                    style: {"fontSize": "15px", "color": "#000", "fontWeight": "bold"}
                },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#BF4149', '#89091F'],
                xAxis: {
                    categories: ['Violations'],
                    labels: {enabled: false},
                    title: {text: false},
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                yAxis: {
                    min: 0,
                    title: {text: null},
                    labels: {enabled: false},
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                legend: {enabled: false},
                plotOptions: {
                    animation: false,
                    colorByPoint: true,
                    column: {stacking: 'normal'},
                    series: {animation: false}
                },
                credits: {enabled: false},
                series: [{
                    name: 'Level 8',
                    data: [level8Total]
                }, {
                    name: 'Level 7',
                    data: [level7Total]
                }, {
                    name: 'Level 6',
                    data: [level6Total]
                }, {
                    name: 'Level 5',
                    data: [level5Total]
                }, {
                    name: 'Level 4',
                    data: [level4Total]
                }, {
                    name: 'Level 3',
                    data: [level3Total]
                }, {
                    name: 'Level 2',
                    data: [level2Total]
                }, {
                    name: 'Level 1',
                    data: [level1Total]
                }]
            }
        });
    }

    loadServiceType(results, type) {
        let eventArr = [];
        for (var i in results) {
            eventArr.push({
                name: results[i]._source.subscriber_service_used,
                data: [results[i]._source.total_notices],
                type: 'column',
                pointWidth: 100
            })
        }
        this.setState({
            methodOfViolationConfig: {
                chart: {
                    type: 'column',
                    height: '157.5',
                    width: chartWidth,
                    style: {fontFamily: 'Roboto,sans-serif', 'outline': '1px solid #D8D8D8'},
                    inverted: true
                },
                title: {
                    text: 'METHOD OF VIOLATION',
                    align: 'left',
                    style: {"fontSize": "15px", "color": "#000", "fontWeight": "bold"}
                },
                colors: ['#FE3E81', '#602220', '#DA536A', '#e47100', '#BA2740', '#F8BACF', '#EA1C62', '#C2165B', '#9D18B2', '#7D0FA4'],
                xAxis: {
                    categories: ['Service Type'],
                    labels: {enabled: false},
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                yAxis: {
                    min: 0,
                    title: {text: null},
                    labels: {enabled: false},
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                legend: {
                    enabled: true,
                    itemStyle: {"fontSize": "11px", "fontWeight": "normal"},
                    symbolHeight: 15,
                    borderWidth: 0,
                    symbolRadius: 0,
                    reversed: true,
                    itemMarginTop: 5
                },
                plotOptions: {
                    animation: false,
                    colorByPoint: true,
                    column: {stacking: 'normal'},
                    series: {animation: false}
                },
                credits: {enabled: false},
                series: eventArr
            }
        });

    }

    loadTrialTotalUploads(results, type) {
        let xAxis = [];
        //Get the x axis Array
        for (var i in results) {
            if (xAxis.indexOf(results[i]._source.time_series_value) == -1)
                xAxis.push(results[i]._source.time_series_value)
        }
        let level8Data = new Array(xAxis.length).fill(0);
        let level1Data = new Array(xAxis.length).fill(0);
        let level2Data = new Array(xAxis.length).fill(0);
        let level3Data = new Array(xAxis.length).fill(0);
        let level4Data = new Array(xAxis.length).fill(0);
        let level5Data = new Array(xAxis.length).fill(0);
        let level6Data = new Array(xAxis.length).fill(0);
        let level7Data = new Array(xAxis.length).fill(0);
        //Now get the
        for (var i in results) {
            for (var x in xAxis) {
                if (xAxis[x] == results[i]._source.time_series_value) {
                    if (results[i]._source.infringement_level_name == "Level 8") {
                        level8Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 1") {
                        level1Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 2") {
                        level2Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 3") {
                        level3Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 4") {
                        level4Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 5") {
                        level5Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 6") {
                        level6Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 7") {
                        level7Data[x] = results[i]._source.total_notices;
                    }
                }
            }
        }

        this.setState({
            totalUploadsConfig: {
                chart: {type: 'column', height: '350', width: '290', style: {"fontFamily": "Roboto,sans-serif","outline": "1px solid #D8D8D8"}},
                title: {
                    text: 'NOTICE TOTAL UPLOAD EVENTS',
                    align: 'left',
                    style: {"fontSize": "15px", "color": "#000", "fontWeight": "bold"}
                },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#BF4149', '#89091F'],
                xAxis: {
                  labels: {
                        rotation: -45
                    },
                    categories: xAxis,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                yAxis: {
                    min: 0,
                    title: {text: null},
                    labels: {enabled: false},
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                legend: {enabled: false},
                plotOptions: {
                    animation: false,
                    colorByPoint: true,
                    column: {stacking: 'normal'},
                    series: {animation: false}
                },
                credits: {enabled: false},
                series: [{name: 'Level 8', data: level8Data}, {name: 'Level 7', data: level7Data}, {
                    name: 'Level 6',
                    data: level6Data
                }, {name: 'Level 5', data: level5Data}, {name: 'Level 4', data: level4Data}, {
                    name: 'Level 3',
                    data: level3Data
                }, {name: 'Level 2', data: level2Data}, {name: 'Level 1', data: level1Data}]
            }
        });
    }

    loadTotalNotices(results, type) {
        console.log("Total notices: ", results);
        this.setState({totalNotices: results[0]._source});

    }

    render() {



        return (
            <Chart
                contentOwner={this.state.contentOwner}
                totalNotices={this.state.totalNotices}
                noticesConfig={this.state.noticesConfig}
                contentTypeConfig={this.state.contentTypeConfig}
                methodOfViolationConfig={this.state.methodOfViolationConfig}
                violationConfig={this.state.violationConfig}
                totalUploadsConfig={this.state.totalUploadsConfig}
                loadCharts={this.loadCharts}
                data={this.props.dataStore}
                onSelect={(val)=> this.onSearchSelect(val)}
            />
        );
    }

}


function mapStateToProps(state) {
    const {dataStore} = state;
    return {dataStore}
}
export default connect(mapStateToProps)(Index);
