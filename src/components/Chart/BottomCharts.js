import React, {Component} from 'react'; // React
const ReactHighcharts = require('react-highcharts');
import LevelSection from './LevelSection'
class BottomCharts extends React.Component {
  render() {
    return (
      <div className="row bottomContent">
          <div className="col-lg-4 paddingLeft7 ContentViolation">

              {/* content Type Chart*/}
              <ReactHighcharts config={this.props.contentTypeConfig}></ReactHighcharts>
              {/* content Type Chart*/} {/* Service Type Chart*/}
              <ReactHighcharts config={this.props.methodOfViolationConfig}></ReactHighcharts>
              {/* Service Type Chart*/}


          </div>

          <LevelSection />

          <div className="col-lg-6 ChartBorder">

              <div className="pull-left ViolationBorderRight col-lg-4">
                  {/* Violation Type Chart*/}
                  <ReactHighcharts config={this.props.violationConfig}></ReactHighcharts>
                  {/* Violation Type Chart*/}
              </div>

              <div className="pull-right col-lg-8 EventBorderNone">
                  {/* Total Upload Events Type Chart*/}
                  <ReactHighcharts config={this.props.totalUploadsConfig}></ReactHighcharts>
                  {/* Total Upload Events Type Chart*/}
              </div>

          </div>

      </div>
    );
  }
}

export default BottomCharts
