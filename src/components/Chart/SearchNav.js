import React, {Component} from 'react'; // React
class SearchNav extends React.Component {
  render() {
    return (
    <div className="search-form">
        <i className="material-icons">mic</i>
        <input type="text"/>
        <i className="material-icons search-button">search</i>
    </div>
    );
  }
}

export default SearchNav
