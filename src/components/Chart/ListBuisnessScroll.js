import React, {Component} from 'react'; // React

class ListBuisnessScroll extends React.Component {

  render() {
    return (
        <div className="flex-container">
          <div className="flex-item pull-left">
            <p className="owner-id owner-pos"> {this.props.item._source.content_owner_name}</p>
          </div>
          <div className="flex-item pull-right">
            <p className="owner-value">{this.props.item._source.total_notices}</p>
          </div>
          <div className="clearfix"></div>
        </div>
    );
  }
}

export default ListBuisnessScroll