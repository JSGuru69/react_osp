import React, {Component} from 'react'; // React
const ReactHighcharts = require('react-highcharts');
import Sidebar from '../../components/NavMenu/SidebarDashboard';
import SliderNav from './SliderNav'
import SearchNav from './SearchNav'
import SliderCharts from './SliderCharts'
import BottomCharts from './BottomCharts'

import Search from '../Search'

class Chart extends React.Component {

    constructor(props) {
        super(props);

        this.state = {}
    }

    render() {

        let searchContainer;
        if (!_.isEmpty(this.props.data.users)) {
            searchContainer =
                <Search
                    data={this.props.data}
                    onSelect={(val)=> this.props.onSelect(val)}
                />
        }

        return (
            <div className="dashboard">
                <Sidebar/>
                <div className="dashboard-content">
                    <div className="title">
                        Copyright Infringement Team: Dashboard
                    </div>
                    {searchContainer}
                    <SliderNav loadCharts={this.props.loadCharts}/>

                    <div className="Graphcontainer container">

                        <SliderCharts
                            contentOwner={this.props.contentOwner}
                            totalNotices={this.props.totalNotices}
                            noticesConfig={this.props.noticesConfig}
                        />

                        <BottomCharts
                            contentTypeConfig={this.props.contentTypeConfig}
                            methodOfViolationConfig={this.props.methodOfViolationConfig}
                            violationConfig={this.props.violationConfig}
                            totalUploadsConfig={this.props.totalUploadsConfig}
                        />

                    </div>

                </div>
            </div>
        );
    }
}

export default Chart
