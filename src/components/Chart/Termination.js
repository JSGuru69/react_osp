import React, {Component} from 'react'; // React
class Termination extends React.Component {

  render() {
    var indicator;
    if(this.props.totalNotices.termination_change_indicator){
           indicator='src/assets/images/'+this.props.totalNotices.termination_change_indicator+'.svg';
    }
    return (
      <div>

          <div className="terminations">
              <ul>
                  <li>
                      <div>
                          <p className="total-hd-txt pull-left font-18"> Total Terminations </p>
                          <p className="pull-right"><span className="total-value font-20"> {this.props.totalNotices.termination_count} </span></p>
                          <div className="clearfix"></div>
                      </div>
                      <div>
                          <p className="mt10 lh50 pull-left"><span className="total-per font-28"> {this.props.totalNotices.termination_change_pct}% </span></p>
                          <p className="pull-right DownUpicon"><img src={indicator} className="status-icon" /></p>
                          <div className="clearfix"></div>
                      </div>
                  </li>
                  <li>
                      <div>
                          <p className="total-hd-txt pull-left font-18"> Total Reinstate </p>
                          <p className="pull-right "><span className="total-value font-20"> {this.props.totalNotices.reinstate_count} </span></p>
                          <div className="clearfix"></div>
                      </div>
                      <div>
                          <p className="mt10 lh50 pull-left"><span className="total-per font-28"> {this.props.totalNotices.reinstate_change_pct}% </span></p>
                          <p className="pull-right DownUpicon"><img src={indicator} className="status-icon" /></p>
                          <div className="clearfix"></div>
                      </div>
                  </li>

                  <li>
                      <div>
                          <p className="total-hd-txt pull-left font-18"> Total Level Promotions </p>
                          <p className="pull-right"><span className="total-value font-20"> {this.props.totalNotices.promotion_count} </span></p>
                          <div className="clearfix"></div>
                      </div>
                      <div>
                          <p className="mt10 lh50 pull-left"><span className="total-per font-28"> {this.props.totalNotices.promotion_change_pct}% </span></p>
                          <p className="pull-right DownUpicon"><img src={indicator} className="status-icon" /></p>
                          <div className="clearfix"></div>
                      </div>
                  </li>
              </ul>
          </div>

      </div>
    );
  }
}

export default  Termination;
