import React, {Component} from 'react'; // React
import ListBuisnessScroll from './ListBuisnessScroll'
class BuisnessSection extends React.Component {

  render() {
    return (
      <div>

              <div className="ABC_section">
                <div className="fleft business-scroll">
                  {
                      this.props.contentOwner.map(function(result,index) {
                      return <ListBuisnessScroll key={index}    item={result} />
                     })
                  }
                </div>
              </div>
              <div className="business-chart">
                <div className="business-content">
                    <p className="margin-zero full-width pad-align btm-border pull-left font-18"><span className="notice-hd">Business</span></p>
                    <p className="pull-right font-24"><span className="notice-value"> {this.props.totalNotices.business_notictes}</span></p>
                    <div className="clearfix"></div>
                </div>
                <div className="business-content">
                    <p className="margin-zero full-width pad-align pull-left font-18"><span className="notice-hd">Residential</span></p>
                    <p className="pull-right font-24"><span className="notice-value"> {this.props.totalNotices.residential_notices}</span></p>
                    <div className="clearfix"></div>
                </div>
                <div className="business-content">
                  <p className="text-center owner-id pull-left font-18"><span className="notice-hd">Total</span></p>
                  <p className="text-center total-per pull-right font-24"><span className="notice-value">{this.props.totalNotices.total_notices}</span></p>
                  <div className="clearfix"></div>
                </div>

              </div>

    </div>
    );
  }
}

export default BuisnessSection
