import React, {Component} from 'react'; // React
class LevelSection extends React.Component {

  render() {
    return (
      <div className="level-section col-lg-2">
          <div className="flex-item pull-left">
              <div className="legend-wrap">
                  <p className="legend-box l8-color"></p>
                  <p className="legend-txt">Level 8</p>
              </div>
              <div className="legend-wrap">
                  <p className="legend-box l7-color"></p>
                  <p className="legend-txt">Level 7</p>
              </div>
              <div className="legend-wrap">
                  <p className="legend-box l6-color"></p>
                  <p className="legend-txt">Level 6</p>
              </div>
              <div className="legend-wrap">
                  <p className="legend-box l5-color"></p>
                  <p className="legend-txt">Level 5</p>
              </div>
              <div className="legend-wrap">
                  <p className="legend-box l4-color"></p>
                  <p className="legend-txt">Level 4</p>
              </div>
              <div className="legend-wrap">
                  <p className="legend-box l3-color"></p>
                  <p className="legend-txt">Level 3</p>
              </div>
              <div className="legend-wrap">
                  <p className="legend-box l2-color"></p>
                  <p className="legend-txt">Level 2</p>
              </div>
              <div className="legend-wrap">
                  <p className="legend-box l1-color"></p>
                  <p className="legend-txt">Level 1</p>
              </div>
          </div>

      </div>
    );
  }
}

export default LevelSection
