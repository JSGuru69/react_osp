import React, {Component} from 'react'; // React
import SliderContent from './SliderContent'
class SliderCharts extends React.Component {
  render() {
    return (
      <div>
          {/*Slider Integeration Code*/}
          <div className="flexslider">
              <ul className="slides">
                  <li>
                      <SliderContent contentOwner={this.props.contentOwner} noticesConfig={this.props.noticesConfig} totalNotices={this.props.totalNotices} />
                  </li>
                  <li>
                      <SliderContent contentOwner={this.props.contentOwner} noticesConfig={this.props.noticesConfig} totalNotices={this.props.totalNotices} />
                  </li>
                  <li>
                      <SliderContent contentOwner={this.props.contentOwner} noticesConfig={this.props.noticesConfig} totalNotices={this.props.totalNotices} />
                  </li>
                  <li>
                      <SliderContent contentOwner={this.props.contentOwner} noticesConfig={this.props.noticesConfig} totalNotices={this.props.totalNotices} />
                  </li>
              </ul>
          </div>
          {/*Slider Integeration Code*/}


      </div>

    );
  }
}

export default  SliderCharts;
