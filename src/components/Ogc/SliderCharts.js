import React, {Component} from 'react'; // React
import SliderContent from './SliderContent'
class SliderCharts extends React.Component {
  render() {
    return (
      <div>
          {/*Slider Integeration Code*/}
          <div className="flexslider">
              <ul className="slides">
                  <li>
                      <SliderContent noticesChartConfig={this.props.noticesChartConfig} totalNotices={this.props.totalNotices} mitigationChartConfig={this.props.mitigationChartConfig} avgViolationChartConfig={this.props.avgViolationChartConfig} />
                  </li>
                  <li>
                      <SliderContent noticesChartConfig={this.props.noticesChartConfig} totalNotices={this.props.totalNotices} mitigationChartConfig={this.props.mitigationChartConfig} avgViolationChartConfig={this.props.avgViolationChartConfig} />
                  </li>
                  <li>
                      <SliderContent noticesChartConfig={this.props.noticesChartConfig} totalNotices={this.props.totalNotices} mitigationChartConfig={this.props.mitigationChartConfig} avgViolationChartConfig={this.props.avgViolationChartConfig} />
                  </li>
                  <li>
                      <SliderContent noticesChartConfig={this.props.noticesChartConfig} totalNotices={this.props.totalNotices} mitigationChartConfig={this.props.mitigationChartConfig} avgViolationChartConfig={this.props.avgViolationChartConfig} />
                  </li>
              </ul>
          </div>
          {/*Slider Integeration Code*/}


      </div>

    );
  }
}

export default  SliderCharts;
