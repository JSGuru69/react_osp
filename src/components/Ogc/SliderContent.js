import React, {Component} from 'react'; // React
const ReactHighcharts = require('react-highcharts');
import BuisnessSection from './BuisnessSection'
import Termination from '../Chart/Termination'

class SliderContent extends React.Component {

  render() {
    return (
      <div className="row TopContent">
          <div className="col-lg-3 padding-0 eq-height">
              <BuisnessSection  avgViolationChartConfig={this.props.avgViolationChartConfig} totalNotices={this.props.totalNotices} />
          </div>
          {/* Notice Type Chart*/}
          <div className="col-lg-6 padding-0 eq-height">
              <ReactHighcharts config={this.props.noticesChartConfig}></ReactHighcharts>

          </div>
          {/* Notice Type Chart*/}
          <div className="col-lg-3 padding-0 eq-height">
              <Termination totalNotices={this.props.totalNotices} />

          </div>

      </div>
    );
  }
}

export default  SliderContent;
