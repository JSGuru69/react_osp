import React, {Component} from 'react'; // React
const ReactHighcharts = require('react-highcharts');
class BottomCharts extends React.Component {
  render() {
    return (
      <div className="row bottomContent">
            <ReactHighcharts config={this.props.mitigationChartConfig}></ReactHighcharts>

      </div>
    );
  }
}

export default BottomCharts
