import React, {Component} from 'react'; // React
const ReactHighcharts = require('react-highcharts');
import SliderNav from '../Chart/SliderNav'
import SliderCharts from './SliderCharts'
import Sidebar from '../../components/NavMenu/SidebarDashboard';
import Search from '../Search'
import BottomCharts from './BottomCharts'

class Chart extends React.Component {

    constructor(props) {
        super(props);

        this.state = {}
    }

    render() {


        return (
            <div className="dashboard">
                <Sidebar/>
                <div className="dashboard-content">
                    <div className="title">
                         Analytics: Dashboard
                    </div>
                    <Search
                        data={this.props.data}
                        onSelect={(val)=> this.props.onSelect(val)}
                    />
                    <SliderNav loadCharts={this.props.loadCharts}/>

                    <div className="Graphcontainer container">

                        <SliderCharts
                            noticesChartConfig={this.props.noticesChartConfig}
                            totalNotices={this.props.totalNotices}
                            mitigationChartConfig={this.props.mitigationChartConfig}
                            avgViolationChartConfig={this.props.avgViolationChartConfig}
                        />
                        <BottomCharts
                            mitigationChartConfig={this.props.mitigationChartConfig}
                            />

                    </div>

                </div>
            </div>
        );
    }
}

export default Chart
