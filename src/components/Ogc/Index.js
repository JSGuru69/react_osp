import React, { Component } from "react";
const ReactHighcharts = require('react-highcharts');
import {connect} from 'react-redux';
import {dataAction} from '../../redux/action'; // Redux Action
import {Link} from 'react-router';
import '../Chart/_style.scss';
import Chart from './Chart'

let jQuery = require('jquery');
require('flexslider');
let chartWidth:361.20000000000005;

class Index extends Component {
  constructor(props) {
    super(props);
    this.prepareChart = this.prepareChart.bind(this);
    this.loadTotalNotices = this.loadTotalNotices.bind(this);
    this.loadNoticesChart = this.loadNoticesChart.bind(this);
    this.loadCharts = this.loadCharts.bind(this);
    this.setChartConfig = this.setChartConfig.bind(this);
    this.initSlider = this.initSlider.bind(this);
    this.onSearchSelect = this.onSearchSelect.bind(this);



    this.state = {
        charts:{},
        totalNoticesDTD:{},
        totalNoticesChartDTD:{},
        chartsDTD:{},

        totalNoticesQTD:{},
        totalNoticesChartQTD:{},
        chartsQTD:{},

        totalNoticesMTD:{},
        totalNoticesChartMTD:{},
        chartsMTD:{},

        totalNoticesYTD:{},
        totalNoticesChartYTD:{},
        chartsYTD:{},
        //current Configurations
        noticesChartConfig:{},
        mitigationChartConfig:{},
        avgViolationChartConfig:{},
        totalNotices:{}
    }
  }

  componentDidMount() {
    fetch('/ogc.json')
    .then(response => {
        return response.json();
    })
    .then(charts => {
      console.log(charts);
        this.setState({ charts });
        this.prepareChart(this.state.charts);
    });

      this.slider = $('.flexslider').flexslider({
      animation: "slide",
      controlNav: false,
      directionNav:false,
      animationLoop: false,
      slideshow: false
    });

    this.action('','users')
  }

  onSearchSelect(val) {
      this.action(val,'saveSelectData')
  }

  action(params, actionItem) {
      const {dispatch} = this.props,
          actionList = {
              source: actionItem,
              params: params,
              actions: {
                  users: {
                      request: {
                          method:'GET',
                          url: 'users',
                          params: {
                              path: 'users'
                          }
                      },
                      success: {
                          triggers: ['titles']
                      },
                      targetPath: 'users'
                  },
                  titles: {
                      request: {
                          method:'GET',
                          url: 'titles',
                          params: {
                              path: 'titles'
                          }
                      },
                      targetPath: 'titles'
                  },
                  saveSelectData: {
                      value: params,
                      targetPath: 'searchSelect'
                  }
              }
          };
      dispatch(dataAction(actionList));
  }


  initSlider(initPoint){
     this.slider.data('flexslider').flexAnimate(initPoint);

  }
  prepareChart(response){
      this.setState({
         totalNoticesDTD :JSON.parse(response.totalNoticesDTD),
         totalNoticesChartDTD : JSON.parse(response.totalNoticesChartDTD),
         chartsDTD : JSON.parse(response.chartsDTD),
    });

     this.loadCharts('D');

  }

  loadCharts(type){
    console.log(type);
    switch (type) {
      case "Y":
          this.initSlider(0);
          if (this.state.totalNoticesYTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesYTD.hits.hits, type);
          if (this.state.totalNoticesChartYTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartYTD.hits.hits, type);
          if (this.state.chartsYTD.hasOwnProperty("hits")) this.setChartConfig(this.state.chartsYTD.hits.hits, type);
          break;
      case "Q":
          this.initSlider(1);
          if (this.state.totalNoticesQTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesQTD.hits.hits, type);
          if (this.state.totalNoticesChartQTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartQTD.hits.hits, type);
          if (this.state.chartsQTD.hasOwnProperty("hits")) this.setChartConfig(this.state.chartsQTD.hits.hits, type);
          break;
      case "M":
          this.initSlider(2);
          if (this.state.totalNoticesMTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesMTD.hits.hits, type);
          if (this.state.totalNoticesChartMTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartMTD.hits.hits, type);
          if (this.state.chartsMTD.hasOwnProperty("hits")) this.setChartConfig(this.state.chartsMTD.hits.hits, type);
          break;

      case "D":
          this.initSlider(3);
          if (this.state.totalNoticesDTD.hasOwnProperty("hits")) this.loadTotalNotices(this.state.totalNoticesDTD.hits.hits, type);
          if (this.state.totalNoticesChartDTD.hasOwnProperty("hits")) this.loadNoticesChart(this.state.totalNoticesChartDTD.hits.hits, type);
          if (this.state.chartsDTD.hasOwnProperty("hits")) this.setChartConfig(this.state.chartsDTD.hits.hits, type);
          break;

      default:

    };

  }


      loadTotalNotices(results, type) {
          console.log("Total notices: ", results);
          this.setState({ totalNotices:results[0]._source });

      }

      loadNoticesChart(results, type){
        let self = this;
        let xAxis = [];
        for (var i in results) {
            if (xAxis.indexOf(results[i]._source.time_series_value) == -1)
                xAxis.push(results[i]._source.time_series_value)
        }

        let forecastData = [];
        let actualData = [];
        for (var item in results) {
            // let dataItem = [results[item]._source.infringement_level];
            // console.log(dataItem);
            forecastData.push(results[item]._source.forecast_notices);
            actualData.push(results[item]._source.actual_notices);
        }

        this.setState({
          noticesChartConfig: {
              chart: {type: 'spline', backgroundColor: '#FFFFFF', width: '445', height: '400'},
              colors: ['#DE7427', '#A62024'],
              title: {text: '', align: 'left', style: {"color": "#fff"}},
              xAxis: {labels: {enabled: false}, tickWidth: 0},
              yAxis: {
                  title: {text: null},
                  labels: {enabled: false},
                  lineWidth: 0,
                  minorGridLineWidth: 0,
                  gridLineColor: '#D8D8D8',
                  minorTickLength: 0,
                  tickLength: 0
              },
              credits: {enabled: false},
              tooltip: {headerFormat: '',},
              plotOptions: {series: {marker: {enabled: true}}},
              series: [{data: forecastData, showInLegend: false, name: "Forecast", lineWidth: 6}, {
                  data: actualData,
                  showInLegend: false,
                  name: "Actual",
                  lineWidth: 6
              }]
          }
  });

      }

      setChartConfig(results, type){
        console.log(results);
        let self = this;
        let xAxisMitigation = [];
        let xAxisAvg = [];
        for (var i in results) {
            if (xAxisMitigation.indexOf(results[i]._source.infringement_level_name) == -1) {
                if (results[i]._source.infringement_level_name != null)
                    xAxisMitigation.push(results[i]._source.infringement_level_name)
            }
            if (xAxisAvg.indexOf(results[i]._source.infringement_level_value) == -1) {
                if (results[i]._source.infringement_level_value != null)
                    xAxisAvg.push(results[i]._source.infringement_level_value)
            }
        }

        let nonRepeat = [];
        let repeat = [];
        let avgViolation = [];
        for (var item in results) {
            if (results[item]._source.infringement_level_value != null || results[item]._source.infringement_level_name != null) {
                nonRepeat.push(results[item]._source.non_repeat_pct);
                repeat.push(results[item]._source.repeat_pct);
                avgViolation.push(results[item]._source.avg_notices);
            }
        }
        var elem = document.querySelector('.search-exp').clientWidth;
        var width = elem - (elem * 0.02);

          this.setState({
              mitigationChartConfig: {
            chart: { type: 'column', backgroundColor: '#fff', width: width, height: '248' },
            colors: ['#CE6A72', '#B31A26'],
            title: { text: 'MITIGATION SUCCESS', align: 'left', style: { "color": "#000", "fontSize": "18px" } },
            xAxis: { categories: xAxisMitigation, reversed: true, tickWidth: 0, lineWidth: 0 },
            yAxis: {
                title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0,
                tickLength: 0,
                stackLabels: {enabled: true,style: {fontWeight: 'bold',color: 'gray'} }
            },
            legend: { layout: 'vertical', align: 'left', x: -10, verticalAlign: 'top', y: 40, margin: 30, symbolRadius: 0, reversed: true, borderWidth: 0, symbolHeight: 15, itemMarginTop: 6, itemStyle: { "fontSize": "12px", "fontWeight": "normal" } },
            credits: { enabled: false },
            tooltip: { headerFormat: '', },
            plotOptions: {
                column: { stacking: 'percent', borderWidth: 0 }
            },
            series: [{ name: 'Non-Repeat', data: nonRepeat, showInLegend: true }, { name: 'Repeat', data: repeat, showInLegend: true }]

        }
      });

        var wid = this.chartThree + 90;
        this.setState({
            avgViolationChartConfig: {
            chart: { type: 'bar', inverted: true, height: '344', width: wid, backgroundColor: '#FFFFFF', style: {fontFamily: 'Roboto,sans-serif', 'outline': '1px solid #D8D8D8'}},
            colors: ['#A51E22'],
            title: { text: 'AVERAGE NUMBER OF VIOLATIONS/LEVEL', align:'center',style: { "color": "#fff", "fontSize": "15px" } },
            xAxis: { categories: xAxisAvg, tickWidth: 0, labels: { style: { color: '#fff' } } },
            yAxis: { labels: { enabled: false }, title: { text: null } },
            plotOptions: {series: {borderColor: '#FFC31F', pointWidth: 26}},
            credits: { enabled: false },
            series: [{ name: "Average Violations", data: avgViolation, showInLegend: false }]
        }
      });

      }

  render() {
    console.log(this.state);

    return (
      <Chart
          noticesChartConfig={this.state.noticesChartConfig}
          mitigationChartConfig={this.state.mitigationChartConfig}
          avgViolationChartConfig={this.state.avgViolationChartConfig}
          totalNotices={this.state.totalNotices}
          loadCharts={this.loadCharts}
          data={this.props.dataStore}
          onSelect={(val)=> this.onSearchSelect(val)}
      />
    );
  }
}

function mapStateToProps(state) {
    const {dataStore} = state;
    return {dataStore}
}
export default connect(mapStateToProps)(Index);
