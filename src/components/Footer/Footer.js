import React from 'react'; // React
import '../../assets/styles/footer.scss';

const Footer=()=>{
    return (

        <div className="row footer">
            <div className="container">
                <div className="col-md-12">
                    <img className="logoimg" src="/src/assets/images/logo.gif" />
                    <span>© PwC LLP (Confidential)</span>
                    <h1>Powered by Rainfall<sup>TM</sup></h1>
                </div>
            </div>
        </div>
    );

}

export default Footer;