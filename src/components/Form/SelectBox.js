import React, {Component} from 'react'; // React
import classNames from 'classnames';
import _ from 'underscore';

class SelectBox extends Component {
    constructor(props) {
        super(props);
        [
            'handleClick',
            'focus',
            'blur'
        ].forEach((name) => this[name] = this[name].bind(this))

        this.state = {
            ...this.props
        }
    }

    componentDidMount() {
        var setTitle = '';
        if (this.props.value[0]) {
            setTitle = _.findWhere(this.props.options, {[this.props.displayValue]:  this.props.value[0]})[this.props.displayName];
        }

        this.setState({
            captionDown: this.state.selectValue || this.props.value[0] ? true : false,
            selectValue: setTitle
        });
    }

    focus() {
        this.setState({
            captionDown: true
        })
    }

    blur() {
        this.setState({
            captionDown: this.state.selectValue ? true : false
        })
    }

    handleClick(val) {
        if(this.props.multiple) {
            this.setState({
                ...this.state,
                value: val,
                captionDown: true
            });

        } else {
            this.setState({
                ...this.state,
                value: {
                    value: val[this.state.displayValue],
                    valueArray: [val[this.state.displayValue]],
                    valueItems: val
                },
                selectValue: val[this.state.displayName],
                captionDown: true
            });
            this.props.onChange(
                {
                    value: val[this.state.displayValue],
                    valueArray: [val[this.state.displayValue]],
                    valueItems: val
                }
            )
        }

    }

    render() {

        console.log('==>',this.props)

        let {
            label,
            title,
            caption,
            id,
            name,
            disabled,
            captionDown,
            value,
            options,
            multiple,
            errorMessage,
            textMessage,
            noMargin,
            displayValue,
            displayName,
            selectValue
        } = this.state;

        var inputClassContainer = classNames('g-select-box', {
            'lg': true,
            'captionDown': captionDown,
            'errorOn': errorMessage,
            'textOn': textMessage,
            'titleOn': title,
            'disabled': disabled,
            'no-margin': noMargin,
            'with-label': label,
            'input-title': title,
            // 'labelOn'       : title ? false : true,
            'withLabel': label
        });

        var renderHtml,
            _this = this;

        if (multiple) {
            renderHtml =
                <div className="dropdown-menu">
                    <form>
                        {
                            options.map(function (item, i) {
                                return (
                                    <div className="g-dd-item" key={i} onClick={()=> {_this.handleClick(item)}}>
                                        <i className="material-icons">done</i>
                                        {item[displayName]}
                                    </div>
                                );
                            }, this)

                        }
                    </form>
                </div>
        } else {
            renderHtml =
                <div className="dropdown-menu">
                    {
                        options.map(function (item, i) {
                            return (
                                <div className="g-dd-item" key={i} onClick={()=> {_this.handleClick(item)}}>
                                    <i className="material-icons">done</i>
                                    {item[displayName]}
                                </div>
                            );
                        }, this)

                    }
                </div>
        }
        return (
            <div className={inputClassContainer}>
                <label>{title}</label>
                <div className="dropdown">
                    <div className="selectboxbutton dropdown-toggle" type="button" data-toggle="dropdown">
                        {selectValue}
                    </div>
                    {renderHtml}
                </div>
                <div className="caption-container" id={"g_caption_id_" + id}>
                    {caption}
                </div>
                <div className="error-container" id={"g_error_id_" + id}>
                    {errorMessage}
                </div>
                <div className="text-container" id={"g_text_container_id_" + id}>
                    {textMessage}
                </div>
            </div>
        );
    }
}


export default SelectBox;