import path     from 'path';
import { argv } from 'yargs';
import pkg      from '../package.json';

const config = new Map();

// ------------------------------------
// User Configuration
// ------------------------------------
config.set('dir_src',  'src');
config.set('dir_dist', 'dist');
config.set('dir_test', 'tests');
config.set('coverage_enabled', !argv.watch); // enabled if not in watch mode
config.set('coverage_reporters', [
  { type : 'text-summary' },
  { type : 'html', dir : 'coverage' }
]);

config.set('server_host',  'localhost');
config.set('server_port',  process.env.PORT || 3000);
config.set('production_enable_source_maps', true);
config.set('vendor_dependencies', [
  'jquery',
  'bootstrap',
  'query-string',
  'react',
  'react-dom',
  'react-redux',
  'react-router',
  'redux',
  'react-router-redux',
  'schema-validator',
  'whatwg-fetch',
  'es6-promise',
  'redux-devtools',
  'redux-thunk'
]);

config.set('env', process.env.NODE_ENV);
config.set('service-host', (process.env.SERVICE_HOST === undefined) ? "localhost" : process.env.SERVICE_HOST);
config.set('globals', {
  'process.env'  : {
    'NODE_ENV' : JSON.stringify(config.get('env'))
  },
  'SERVICE_HOST' : JSON.stringify(config.get('service-host')),
  'NODE_ENV'     : config.get('env'),
  '__DEV__'      : config.get('env') === 'development',
  '__PROD__'     : config.get('env') === 'production',
  '__DEBUG__'    : config.get('env') === 'development' && !argv.no_debug,
  '__DEBUG_NW__' : !!argv.nw
});

// Webpack
config.set('webpack_public_path',
    `http://${config.get('webpack_host')}:${config.get('webpack_port')}/`
);

// Project
config.set('path_project', path.resolve(__dirname, '../'));

// Utilities
const paths = (() => {
  const base    = [config.get('path_project')];
  const resolve = path.resolve;
  const project = (...args) => resolve.apply(resolve, [...base, ...args]);
  return {
    project : project,
    src     : project.bind(null, config.get('dir_src')),
    dist    : project.bind(null, config.get('dir_dist'))
  };
})();

config.set('utils_paths', paths);
config.set('utils_aliases', [
  'components',
  'containers',
  'redux',
  'assets'
].reduce((acc, dir) => ((acc[dir] = paths.src(dir)) && acc), {}));
export default config;
