const rules = [
    {
        "rulerIdentifier" : "INF_LE_001",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Subscriber received new infringement notification for the first time",
        "ruleDescription" : "All subscribers begin at Level 0, with no action required by ISP. Upon receiving the first complaint notification, this rule will move subscriber to Level 1 and an email communication should be sent with a notice of the Complaint Notification received pertaining to the account's alleged infringing activity.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_LE_002",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 1 subscriber received a new infringement notification after eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification, the rule will check if the notification received date is during eligible calendar days (7 days for level 1 subscribers). If the notification received after eligible calendar days, then subscriber is moved to Level 2 and an email communication should be sent with a notice of the Complaint Notification received pertaining to the account's alleged infringing activity and an In-Browser notification is displayed",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_LE_003",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 2 subscriber received a new infringement notification after eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification, the rule will check if the notification received date is during eligible calendar days (7 days for level 2 subscribers). If the notification received after eligible calendar days, then subscriber is moved to Level 3 and an email communication should be sent with a notice of the Complaint Notification received pertaining to the account's alleged infringing activity and subscriber is directed to soft-wall garden",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_LE_004",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 3 subscriber received a new infringement notification after eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification, the rule will check if the notification received date is during eligible calendar days (7 days for level 3 subscribers). If the notification received after eligible calendar days, then subscriber is moved to Level 4 and an email communication should be sent with a notice of the Complaint Notification received pertaining to the account's alleged infringing activity and subscriber is directed to soft-wall garden",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_LE_005",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 4 subscriber received a new infringement notification after eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification, the rule will check if the notification received date is during eligible calendar days (14 days for level 4 subscribers). If the notification received after eligible calendar days, then subscriber is moved to Level 5 and an email communication should be sent with a notice of the Complaint Notification received pertaining to the account's alleged infringing activity and subscriber is directed to hard-wall garden",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_LE_006",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 5 subscriber received a new infringement notification after eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification, the rule will check if the notification received date is during eligible calendar days (14 days for level 5 subscribers). If the notification received after eligible calendar days, then subscriber is moved to Level 6 and an email communication should be sent with a notice of the Complaint Notification received pertaining to the account's alleged infringing activity and subscriber is directed to hard-wall garden,Training Video, and 21 days of Soft‐Walled Garden or IBN",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_LE_007",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 6 subscriber received a new infringement notification after eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification, the rule will check if the notification received date is during eligible calendar days (21 days for level 6 subscribers). If the notification received after eligible calendar days, then subscriber is moved to Level 7 and an email communication should be sent with a notice of the Complaint Notification received pertaining to the account's alleged infringing activity and subscriber is directed to hard-wall garden,Training Video, and 21 days of Soft‐Walled Garden or IBN",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_LE_008",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 7 subscriber received a new infringement notification after eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification, the rule will check if the notification received date is during eligible calendar days (21 days for level 7 subscribers). If the notification received after eligible calendar days, then subscriber is moved to Level 8 and advanced notice of the termination date will be sent.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_NA_001",
        "ruleGroup" : "Infringements",
        "subGroup" : "No Action",
        "ruleName" : "Level 1 subscriber received a new infringement notification during eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification during calendar eligible days (7 days for Level 1), subscriber should not be escalated . Subscriber stays in current level (Level 1) but receives an email notification with a compiled list of all alleged infringement activities.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_NA_002",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 2 subscriber received a new infringement notification during eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification during calendar eligible days (7 days for Level 2), subscriber should not be escalated . Subscriber stays in current level (Level 2) but receives an email notification with a compiled list of all alleged infringement activities.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_NA_003",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 3 subscriber received a new infringement notification during eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification during calendar eligible days (7 days for Level 3), subscriber should not be escalated . Subscriber stays in current level (Level 3) but receives an email notification with a compiled list of all alleged infringement activities.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_NA_004",
        "ruleGroup" : "Infringements",
        "subGroup" : "No Action",
        "ruleName" : "Level 4 subscriber received a new infringement notification during eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification during calendar eligible days (14 days for Level 4), subscriber should not be escalated . Subscriber stays in current level (Level 4) but receives an email notification with a compiled list of all alleged infringement activities.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_NA_005",
        "ruleGroup" : "Infringements",
        "subGroup" : "No Action",
        "ruleName" : "Level 5 subscriber received a new infringement notification during eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification during calendar eligible days (14 days for Level 5), subscriber should not be escalated . Subscriber stays in current level (Level 5) but receives an email notification with a compiled list of all alleged infringement activities.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_NA_006",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 6 subscriber received a new infringement notification during eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification during calendar eligible days (21 days for Level 6), subscriber should not be escalated . Subscriber stays in current level (Level 6) but receives an email notification with a compiled list of all alleged infringement activities.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_NA_007",
        "ruleGroup" : "Infringements",
        "subGroup" : "Level Escalation",
        "ruleName" : "Level 7 subscriber received a new infringement notification during eligible calendar days",
        "ruleDescription" : "Upon receiving the complaint notification during calendar eligible days (21 days for Level 7), subscriber should not be escalated . Subscriber stays in current level (Level 7) but receives an email notification with a compiled list of all alleged infringement activities.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_RC_001",
        "ruleGroup" : "Infringements",
        "subGroup" : "Reset Counter",
        "ruleName" : "Level 1 subscriber reset back to Level 0, if no new complaint for 180 calendar days",
        "ruleDescription" : "Subscriber in Level 1 should reset back to Level 0, if no new complaint notifications attributed to that Subscriber are received and processed for 180 calendar days.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_RC_002",
        "ruleGroup" : "Infringements",
        "subGroup" : "Reset Counter",
        "ruleName" : "Level 2 subscriber reset back to Level 0, if no new complaint for 180 calendar days",
        "ruleDescription" : "Subscriber in Level 2 should reset back to Level 0, if no new complaint notifications attributed to that Subscriber are received and processed for 180 calendar days.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_RC_003",
        "ruleGroup" : "Infringements",
        "subGroup" : "Reset Counter",
        "ruleName" : "Level 3 subscriber reset back to Level 0, if no new complaint for 180 calendar days",
        "ruleDescription" : "Subscriber in Level 3 should reset back to Level 0, if no new complaint notifications attributed to that Subscriber are received and processed for 180 calendar days.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "NF_RC_004",
        "ruleGroup" : "Infringements",
        "subGroup" : "Reset Counter",
        "ruleName" : "Level 4 subscriber reset back to Level 0, if no new complaint for 180 calendar days",
        "ruleDescription" : "Subscriber in Level 4 should reset back to Level 0, if no new complaint notifications attributed to that Subscriber are received and processed for 180 calendar days.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_RC_005",
        "ruleGroup" : "Infringements",
        "subGroup" : "Reset Counter",
        "ruleName" : "Level 5 subscriber reset back to Level 0, if no new complaint for 180 calendar days",
        "ruleDescription" : "Subscriber in Level 5 should reset back to Level 0, if no new complaint notifications attributed to that Subscriber are received and processed for 180 calendar days.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_RC_006",
        "ruleGroup" : "Infringements",
        "subGroup" : "Reset Counter",
        "ruleName" : "Level 6 subscriber reset back to Level 0, if no new complaint for 180 calendar days",
        "ruleDescription" : "Subscriber in Level 6 should reset back to Level 0, if no new complaint notifications attributed to that Subscriber are received and processed for 180 calendar days.",
        "functionalArea" : "General Council"
    },
    {
        "rulerIdentifier" : "INF_RC_007",
        "ruleGroup" : "Infringements",
        "subGroup" : "Reset Counter",
        "ruleName" : "Level 7 subscriber reset back to Level 0, if no new complaint for 180 calendar days",
        "ruleDescription" : "Subscriber in Level 7 should reset back to Level 0, if no new complaint notifications attributed to that Subscriber are received and processed for 180 calendar days.",
        "functionalArea" : "General Council"
    },
];

export default rules;