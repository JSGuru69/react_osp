const apis = [

  {
    "title" : "DMCA Validate Task",
    "purpose" : "Validate subscriber information from the input received",
    "element" : "Bulk Upload",
    "apiformat" : "NA",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "DMCA Upload API Task",
    "purpose" : "Validate input received from bulk upload",
    "element" : "Bulk Upload",
    "apiformat" : "CSV File",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "DMCA Webform API Task",
    "purpose" : "Validate input received from Web-Form",
    "element" : "Manual Infringement",
    "apiformat" : "Web-Form",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "Search Suggest API",
    "purpose" : "Provide wild card search capabilities with any name or account of the subscriber. Atleast 3 characters should be typed to trigger this API Ex: Type Dav to return all the matchings as David, Dave etc. Type ISP-R to retrieve all residential accounts",
    "element" : "Dashboard",
    "apiformat" : "http://<ip>:<<port>/subscribers/infringements/_search",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "Search Select API",
    "purpose" : "Display details of the subscriber selected from search",
    "element" : "Dashboard/Portal Landing Page",
    "apiformat" : "http://<ip>:<port>/subscribers/infringements/\" + _sub_id",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "Subscriber Infringement API",
    "purpose" : "Retrieve details of the selected subscriber infringement levels and details",
    "element" : "Dashboard",
    "apiformat" : "http://<ip>:<<port>/subscribers/details/_search?filter_path=hits.hits._source&size=10000&pretty",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "Infringement History Chart API",
    "purpose" : "Provide data for infringement history chart",
    "element" : "Dashboard/Portal Landing Page",
    "apiformat" : "http://<ip>:<port>/subscribers/history/_search?filter_path= hits.hits._source.infringement_level, hits.hits._source.notification_date&size=10000&pretty",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "Total Notices Chart API",
    "purpose" : "Provide input for Subscriber type (i.e Residential, Business) chart",
    "element" : "Dashboard",
    "apiformat" : "http://<<ip>:<port>/charts/noticesbreakdown/_search?filter_path= hits.hits._source&size= 10000&q=period_type:YTD&pretty",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "Service Type Chart API",
    "purpose" : "Provide input for method of violation (i.e BitTorrent, kaaza) chart",
    "element" : "Dashboard/Portal Landing Page",
    "apiformat" : "http://<ip>:<port>/charts/servicetypes/_search?filter_path= hits.hits._source&size=10000&q= period_type:YTD&sort=total_notices:desc&pretty",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  },
  {
    "title" : "Infringement Levels Chart API",
    "purpose" : "Provide input for NOTICE TOTAL UPLOAD EVENTS chart",
    "element" : "Dashboard",
    "apiformat" : "http://<ip>:<port>/charts/avgperlevel_mitigation/_search?filter_path =hits.hits._source&size=10000&sort= infringement_level_value:desc&q=period_type:YTD&pretty",
    "exceptions" : ["400 - Bad Request", "401 - Unauthorized", "404 - Not Found"],
    "outputformat" : "JSON"
  }

]

export default apis